<?php
session_start();
require_once('function_file_get_contents.php');
require_once('../model/Logradouro.php');
require_once('../model/Endereco.php');
require_once('../model/Entidade.php');
require_once('../model/Entidade_Juridica.php');
require_once('../model/Entidade_LocalExecSolic.php');

$chars = array("(",")","-","/","."," ");
$localexec  = new Entidade_LocalExecSolic;
$ent        = new Entidade;
$end        = new Endereco;
$entJur     = new Entidade_Juridica;
$logra      = new Logradouro;
//$entidade   = $_SESSION['identidade'];
//$localexec->set_Identidade_operadora(0);

if(! empty($_POST)){
    if(isset($_POST['identidade'])){
     $ent->set_Identidade($_POST['identidade']);
    // $ent->set_Identidade(110);
    }
}else{
   $ent->set_Identidade(0);
}
if(! empty($_POST)){
    if(isset($_POST['idlogradouro'])){
   $logra->set_Idlogradouro($_POST['idlogradouro']);
    }
}else{
  
 // $logra->set_Idlogradouro($_POST['idlogradouro']);
}
if(!empty($_POST['tipolocal'])){
    if(isset($_POST['tipolocal'])){
   $ent->set_Idtipo($_POST['tipolocal']);
    }
}else{
  
  $ent->set_Idtipo('');
}
if(!empty($_POST['tipoconvenio'])){

    $localexec->set_Identidade_operadora($_POST['tipoconvenio']);
}else{
    $localexec->set_Identidade_operadora(0);
}

if(!empty($_POST)){
    if(isset($_POST['nomel'])){
        $ent->set_Nome($_POST['nomel']);
    }
}else{
  
    $ent->set_Nome($_POST['nomel']);
}
if(! empty( $_POST)){
    if(isset($_POST['nomefantasial'])){
    $entJur->set_Nomefantasia($_POST['nomefantasial']);
    }
}else{
    //$entJur->set_Nomefantasia($_POST['nomefantasial']);
}
if(! empty( $_POST)){
    if(isset($_POST['cnpjl'])){
    $entJur->set_Cnpj(str_replace($chars,'',$_POST['cnpjl']));
    }
}else{
   //$entJur->set_Cnpj("");
}
if(! empty( $_POST)){
    if(isset($_POST['inscr_municipall'])){
    $entJur->set_Inscr_municipal($_POST['inscr_municipall']);
    }
}else{
    $inscr_municipal="";
}
if(! empty( $_POST)){
    if(isset($_POST['inscr_estaduall'])){
    $entJur->set_Inscr_estadual($_POST['inscr_estaduall']);
    }
}else{
    $inscr_estadual="";
}

if(! empty( $_POST)){
    if(isset($_POST['cnesl'])){
    $entJur->set_Cnes($_POST['cnesl']);
    }
}else{
    $cnes="";
}

if(! empty( $_POST)){
    if(isset($_POST['nomecontatol'])){
    $ent->set_Nomecontato($_POST['nomecontatol']);
    }
}else{
    $nomecontato="";
}

if(!empty($_POST)){
    if(isset($_POST['fonel'])){
    $ent->set_Fone(str_replace($chars,'',$_POST['fonel']));
    }
   
}else{
    $fone="";
}
if(!empty($_POST)){
    if(isset($_POST['emaill'])){
    $ent->set_Email($_POST['emaill']);
    }
}else{
    $email="";
}
if(!empty($_POST)){
    if(isset($_POST['celularl'])){
    $ent->set_Celular(str_replace($chars,'',$_POST['celularl']));
    }
}else{
    $celular="";
}

if(! empty( $_POST)){
    if(isset($_POST['flg_status'])){
    $ent->set_Flg_status($_POST['flg_status']);
    }
}else{
    
}
  if(!empty($_POST)){
    if(isset($_POST['identidade'])){
        $ent->set_Identidade_vinculo($_POST['identidade']);
       // $ent->set_Identidade_vinculo(110);
    }
    }else{
       $ent->set_Identidade_vinculo(0);
    }    

if(! empty( $_POST)){
    if(isset($_POST['flg_hub'])){
    $entJur->set_Flg_hub($_POST['flg_hub']);
    }
}else{
    $flg_hub="";
}
if(! empty( $_POST)){
    if(isset($_POST['logo'])){
    $entJur->set_Logo($_POST['logo']);
    }
}else{
    $logo="";
}
if(! empty( $_POST)){
    if(isset($_POST['dtultvalidacaonet'])){
    $entJur->set_Dtultvalidacaonet($_POST['dtultvalidacaonet']);
    }
}else{
    $dtultvalidacaonet="";
}
if(! empty( $_POST)){
    if(isset($_POST['codigoibge'])){
    $entJur->set_Codigoibge($_POST['codigoibge']);
    }
}else{
    $codigoibge="";
}
if(! empty( $_POST)){
    if(isset($_POST['contrato'])){
    $entJur->set_Contrato($_POST['contrato']);
    }
}else{
    $contrato="";
}
if(! empty( $_POST)){
    if(isset($_POST['limitediasagenda'])){
    $entJur->set_Limitediasagenda($_POST['limitediasagenda']);
    }
}else{
    $limitediasagenda="";
}

if(! empty( $_POST)){
    if(isset($_POST['timerparaagendamento'])){
    $entJur->set_Timerparaagendamento($_POST['timerparaagendamento']);
    }
}else{
    $timerparaagendamento="";
}
if(! empty( $_POST)){
    if(isset($_POST['identidade_matriz'])){
    $entJur->set_Identidade_matriz($_POST['identidade_matriz']);
    }
}else{
    $identidade_matriz="";
}

if(! empty($_POST)){
    if(isset($_POST['idenderecol'])){
     $end->set_Idendereco($_POST['idenderecol']);
    }
}else{
  
  }
  if(! empty($_POST)){
    if(isset($_POST['dsenderecol'])){
     $end->set_Dsendereco($_POST['dsenderecol']);
    }
}else{
  
  }
 if(! empty($_POST)){
    if(isset($_POST['nrenderecol'])){
   $end->set_Nrendereco($_POST['nrenderecol']);
    }
}else{
  
  }
   if(! empty($_POST)){
    if(isset($_POST['complementol'])){
   $end->set_Complemento($_POST['complementol']);
    }
}else{
  
  }

if(! empty( $_POST)){
    if(isset($_POST['cepl'])){
    $end->set_Cep($_POST['cepl']);
    }
}else{
    $cep="";
}

if(! empty( $_POST)){
    if(isset($_POST['bairrol'])){
    $end->set_Bairro($_POST['bairrol']);
    }
}else{
    $bairro="";
}
if(! empty( $_POST)){
    if(isset($_POST['cidadel'])){
    $end->set_Cidade($_POST['cidadel']);
    }
}else{
    $cidade="";
}
if(! empty( $_POST)){
    if(isset($_POST['ufl'])){
    $end->set_Uf($_POST['ufl']);
    }
}else{
    $uf="";
}
if(! empty( $_POST)){
    if(isset($_POST['identidade_operadora'])){    
    }
}else{

     $localexec->set_Identidade_operadora(0);
}

if(!empty($_POST['cod_operadora'])){

    $localexec->set_Codoperadora($_POST['cod_operadora']);
}else{
    $localexec->set_Codoperadora(0);
}

if(! empty( $_POST)){
    if(isset($_POST['tipoenderecol'])){
    $end->set_Idtipoendereco($_POST['tipoenderecol']);
    }
}else{
   $end->set_Idtipoendereco(0);
}

header("Location: ../cadempresa.php");

$data = array(
        'entidade'           => $ent, 
        'entidadejuridica'   => $entJur,
        'locaisExec'         => $localexec,
        'endereco'           => $end
               
);

$url = "http://localhost:6760/WcfCiHealth/Entidade/CadastrarEntidadeJuridica";
$content = json_encode($data);
//echo $content;

$json_response = file_get_contents_curl2($url,$content);
// require_once('CamposSessao.php');
 
$id =0 ;
$response = json_decode($json_response, true);
foreach ($response as $e) 
    { //echo $e."<br>";
    $_SESSION['identidadeLC'] = $e;  
    
    $id =$e ;
   
    } 
   
?>
