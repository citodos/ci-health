<?php
session_start();

 if(isset($_SESSION['unidade']) != null){
    $identidade     = $_SESSION['unidade'];
    $nome            = $_SESSION['nome'];
    $nomefantasia    = $_SESSION['nomefantasia'];
    $cnpj            = $_SESSION['cnpj'];
    $inscr_estadual  = $_SESSION['inscr_estadual'];
    $inscr_municipal = $_SESSION['inscr_municipal'];
    $cnes            = $_SESSION['cnes'];
    $nomecontato     = $_SESSION['nomecontato'];
    
  }else{
    $codigo          = "";
    $identidade      = "";
    $nome            = "";
    $nomefantasia    = "";
    $cnpj            = "";
    $inscr_estadual  = "";
    $inscr_municipal = "";
    $cnes            = "";
    $nomecontato     = "";
  }
 
  ?>
<!DOCTYPE html>
<html lang=en>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>

<script language="JavaScript" type="text/javascript" src="assets/js/validacoes/validaempresa.js" ></script>  
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script language="JavaScript" type="text/javascript" src="assets/js/jquery-2.1.1.js" ></script>
<script language="JavaScript" type="text/javascript" src="assets/js/getEndereco.js" ></script> 
<script language="JavaScript">
function getEndereco(cep) {
    if($.trim(cep) != ""){
        $("#loadingCep").html('Pesquisando...');
 
        $.getJSON("pesquisas/pesquisarcep.php",{cep:cep}, function(resultadoCEP){  
         // alert(resultadoCEP["GetPesquisarEnderecoResult"]);
         if ($.isEmptyObject(resultadoCEP["GetPesquisarEnderecoResult"])) {
            alert('Número de cep não encontrado.');

         }else{
           $.each(resultadoCEP["GetPesquisarEnderecoResult"], function(key, val) {
           
             
                $("#dsendereco").val(unescape(val.dsendereco));
                $("#bairro").val(unescape(val.bairro));
                $("#cidade").val(unescape(val.cidade));
                $("#tipoLogradouro").val(unescape(val.Logradouro.dslogradouro));
                document.getElementById("uf").value = val.uf;
           
           });
         };
            
       });
       // $.getScript("http://localhost:6760/WcfCiHealth/Endereco/Pesquisa?idendereco=0&dsendereco=&bairro=&cidade=&uf=&cep="+cep, function(resultadoCEP){            
         
    }else{
        $("#loadingCep").html('Informe o CEP');
    }
}
function buscaEndereco(){
  if($("#cep").val() != ""){
    //alert($("#cep").val());
    getEndereco($("#cep").val());
  }else{
        alert("Favor preencher o campo cep");
  }
}

</script>

<meta charset=utf-8>
<title>Cadastro de Médico | Área Administrativa</title>
<!-- Mobile specific metas -->
<meta name=viewport content="width=device-width,initial-scale=1,maximum-scale=1">
<!-- Force IE9 to render in normal mode -->
<!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
<meta name=author content=SuggeElson>
<meta name=description content="">
<meta name=keywords content="">
<!-- Import google fonts - Heading first/ text second -->
<link rel=stylesheet type=text/css href="http://fonts.googleapis.com/css?family=Open+Sans:400,700|Droid+Sans:400,700">
<!--[if lt IE 9]>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400" rel="stylesheet" type="text/css" />
<link href="http://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet" type="text/css" />
<link href="http://fonts.googleapis.com/css?family=Droid+Sans:400" rel="stylesheet" type="text/css" />
<link href="http://fonts.googleapis.com/css?family=Droid+Sans:700" rel="stylesheet" type="text/css" />
<![endif]-->
<!-- Css files -->
<link rel=stylesheet href='assets/css/main.min.css'>

<link rel=stylesheet href='assets/css/bootstrap.css'>
<!-- Fav and touch icons -->
<link rel='apple-touch-icon-precomposed' sizes='144x144' href='assets/img/ico/apple-touch-icon-144-precomposed.png'>
<link rel='apple-touch-icon-precomposed' sizes='114x114' href='assets/img/ico/apple-touch-icon-114-precomposed.png'>
<link rel='apple-touch-icon-precomposed' sizes='72x72' href='assets/img/ico/apple-touch-icon-72-precomposed.png'>
<link rel='apple-touch-icon-precomposed' href='assets/img/ico/apple-touch-icon-57-precomposed.png'>
<link rel='icon' href='assets/img/ico/favicon.ico' type='image/png'>
<!-- Windows8 touch icon ( http://www.buildmypinnedsite.com/ )-->
<meta name='msapplication-TileColor' content='#3399cc'>
<body>
<?php include ("elements/header.php"); ?>

<?php include ("elements/sidebar.php"); ?>
<!-- Start #right-sidebar -->
<div id='right-sidebar' class='hide-sidebar'>
  <!-- Start .sidebar-inner -->
  <div class='sidebar-inner'>
    <div class="sidebar-panel mt0">
      <div class="sidebar-panel-content fullwidth pt0">
        <div class='chat-user-list'>
          <form class="form-horizontal chat-search" role='form'>
            <div class='form-group'>
              <input class='form-control' placeholder="Search for user...">
              <button type='submit'><i class="ec-search s16"></i></button>
            </div>
            <!-- End .form-group  -->
          </form>
          <ul class="chat-ui bsAccordion">
            <li><a href=#>Favorites <span class="notification teal">4</span><i class='en-arrow-down5'></i></a>
              <ul class='in'>
                <li><a href='#' class='chat-name'><img class='chat-avatar' src='assets/img/avatars/49.jpg' alt=@chadengle>Chad Engle <span class='has-message'><i class='im-pencil'></i></span></a> <span class="status online"><i class='en-dot'></i></span></li>
                <li><a href='#' class='chat-name'><img class='chat-avatar' src='assets/img/avatars/54.jpg' alt='@alagoon'>Anthony Lagoon</a> <span class="status offline"><i class=en-dot></i></span></li>
                <li><a href='#' class='chat-name'><img class='chat-avatar' src='assets/img/avatars/52.jpg' alt='@koridhandy'>Kory Handy</a> <span class='status'><i class='en-dot'></i></span></li>
                <li><a href='#' class='chat-name'><img class='chat-avatar' src='assets/img/avatars/50.jpg' alt='@divya'>Divia Manyan</a> <span class='status'><i class='en-dot'></i></span></li>
              </ul>
            </li>
            <li><a href='#'>Online <span class="notification green">3</span><i class='en-arrow-down5'></i></a>
              <ul class='in'>
                <li><a href='#' class='chat-name'><img class='chat-avatar' src='assets/img/avatars/51.jpg' alt='@kolage'>Eric Hofman</a> <span class="status online"><i class='en-dot'></i></span></li>
                <li><a href='#' class='chat-name'><img class='chat-avatar' src='assets/img/avatars/55.jpg' alt='@mikebeecham'>Mike Beecham</a> <span class="status online"><i class='en-dot'></i></span></li>
                <li><a href='#' class='chat-name'><img class='chat-avatar' src='assets/img/avatars/53.jpg' alt='@derekebradley'>Darek Bradly</a> <span class="status online"><i class='en-dot'></i></span></li>
              </ul>
            </li>
            <li><a href='#'>Offline <span class="notification red">5</span><i class='en-arrow-down5'></i></a>
              <ul>
                <li><a href='#' class='chat-name'><img class='chat-avatar' src='assets/img/avatars/56.jpg' alt='@laurengray'>Lauren Grey</a> <span class="status offline"><i class='en-dot'></i></span></li>
                <li><a href='#' class='chat-name'><img class='chat-avatar' src='assets/img/avatars/49.jpg' alt='@chadengle'>Chad Engle</a> <span class="status offline"><i class='en-dot'></i></span></li>
                <li><a href='#' class='chat-name'><img class='chat-avatar' src='assets/img/avatars/58.jpg' alt='@frankiefreesbie'>Frankie Freesibie</a> <span class="status offline"><i class='en-dot'></i></span></li>
                <li><a href='#' class='chat-name'><img class='chat-avatar' src='assets/img/avatars/57.jpg' alt='@joannefournier'>Joane Fornier</a> <span class="status offline"><i class='en-dot'></i></span></li>
                <li><a href='#' class='chat-name'><img class='chat-avatar' src='assets/img/avatars/59.jpg' alt='@aiiaiiaii'>Alia Alien</a> <span class="status offline"><i class='en-dot'></i></span></li>
              </ul>
            </li>
          </ul>
        </div>
        <div class='chat-box'>
          <h5>Chad Engle</h5>
          <a id='close-user-chat' href='#' class="btn btn-xs btn-primary"><i class='en-arrow-left4'></i></a>
          <ul class="chat-ui chat-messages">
            <li class='chat-user'>
              <p class='avatar'><img src='assets/img/avatars/49.jpg' alt='@chadengle'></p>
              <p class='chat-name'>Chad Engle <span class='chat-time'>15 seconds ago</span></p>
              <span class="status online"><i class='en-dot'></i></span>
              <p class='chat-txt'>Hello Sugge check out the last order.</p>
            </li>
            <li class='chat-me'>
              <p class='avatar'><img src='assets/img/avatars/48.jpg' alt='SuggeElson'></p>
              <p class='chat-name'>SuggeElson <span class='chat-time'>10 seconds ago</span></p>
              <span class="status online"><i class='en-dot'></i></span>
              <p class='chat-txt'>Ok i will check it out.</p>
            </li>
            <li class='chat-user'>
              <p class='avatar'><img src='assets/img/avatars/49.jpg' alt='@chadengle'></p>
              <p class='chat-name'>Chad Engle <span class='chat-time'>now</span></p>
              <span class="status online"><i class='en-dot'></i></span>
              <p class='chat-txt'>Thank you, have a nice day</p>
            </li>
          </ul>
          <div class='chat-write'>
            <form action='#' class='form-horizontal' role='form'>
              <div class='form-group'>
                <textarea name='sendmsg' id='sendMsg' class="form-control elastic" rows=1></textarea>
                <a role='button' class='btn' id='attach_photo_btn'><i class="fa-picture s20"></i></a>
                <input type='file' name='attach_photo' id='attach_photo'>
              </div>
              <!-- End .form-group  -->
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End .sidebar-inner -->
</div>
<!-- Start #content -->
<div id='content'>
   <div class='content-wrapper'>
   
    <div class='row'>
      <!-- Start .row -->
      <!-- Start .page-header -->
        <div class='col-lg-12 heading'>
        <h1 class='page-header'><img src="assets\img\cad_empresa_img.png"></i> Cadastro de Empresas</h1>
    </div>
  </div>
</div></div>

  <div class=clearfix></div>

<div class=tabs>
  <div class=tabs>
            <ul id=myTab2 class="nav nav-tabs nav-justified">
              <li><a href=#empresa data-toggle=tab>Empresa</a></li>
              <li><a href=#endereco data-toggle=tab>Endereço</a></li>

           <!--   <li><a href=#contatos data-toggle=tab>Contatos</a></li>-->
              <li><a href=#locais data-toggle=tab>Locais</a><span class="notification">4</span></li>
          
            </ul>
            <div id=myTabContent2 class=tab-content>
         
                <div class="tab-pane fade active in" id="empresa">
                  
                  <div class="box-empresa" name="box-empresa">
<form  name="frmempresa" method="Post" id="validate"  action="enviarCadastros/cadempresaEnviarDados.php?idtipo=EM" onSubmit="return valida();">


 <div class=form-group>
 <label style="color:#000;"> Dados da Empresa</label>
  <hr>
                  <label class="col-sm-5 control-label"> Nome da Empresa:</label>

                  <label class="col-sm-6 control-label">Nome Fantasia:</label>
           
                  <div class=row>
                    <div class="col-lg-4 col-md-4">
                      <input class=form-control  name="nome" id="nome" value="<?php echo $nome; ?>">

                    </div>
                  <div class="col-lg-1 col-md-1">
           <a href="#" onclick="window.open('buscarEmpresa.php', 'Pagina', 'STATUS=NO, TOOLBAR=NO, LOCATION=NO, DIRECTORIES=NO, RESISABLE=NO, SCROLLBARS=YES, TOP=10, LEFT=10, WIDTH=900, HEIGHT=600');"><img class="btnpesquisaempresa" src="assets\img\search32.png" title="Pesquisar empresa" alt="Pesquisar empresa" ></a>
</div>
                    <div class="col-lg-3 col-md-3" >
                      <input class=form-control name="nomefantasia" id="nomefantasia" value="<?php echo $nomefantasia; ?>">
                    </div>
                  </div>
                </div>
                  
                  <label class="col-sm-3 control-label">CNPJ:</label>
                  <label class="col-sm-3 control-label">Inscrição Municipal:</label>
                  <label class="col-sm-5 control-label">Inscrição Estadual:</label>
                  <div class=row>
                    <div class="col-lg-3 col-md-3">
                      <input class=form-control name="cnpj" onKeyPress="return MascaraCNPJ(frmempresa.cnpj);" 
maxlength="18" onBlur="return ValidarCNPJ(frmempresa.cnpj);" value="<?php echo $cnpj; ?>">
                    </div>
                    <div class="col-lg-3 col-md-3">
                      <input class=form-control name="inscr_municipal" value="<?php echo $inscr_municipal; ?>">
                    </div>
                      <div class="col-lg-3 col-md-3">
                      <input class=form-control name="inscr_estadual" value="<?php echo $inscr_estadual; ?>">
                  
              </div>
                  </div>
<br>

                  <label class="col-sm-11 control-label">CNES:</label>
                 
                  <div class=row>
                   
                    <div class="col-lg-3 col-md-3">
                      <input class=form-control name="cnes" value="<?php echo $cnes; ?>">
                    </div>                    </div>
<br>
 <label style="color:#000;"> Contato da Empresa</label>
  <hr>
                  <label class="col-sm-3 control-label">Nome do Contato:</label>

                  <label class="col-sm-2 control-label">Telefone:</label>
          <label class="col-sm-2 control-label">Celular:</label>
                 <label class="col-sm-5 control-label">E-mail:</label>
      
       
                  <div class=row>
                   
                    <div class="col-lg-3 col-md-3">
                      <input class=form-control name="nomecontato" id="nomecontato" value="<?php echo $nomecontato; ?>">
                    </div>
                     <div class="col-lg-2 col-md-2">
                      <input class=form-control name="fone" id="fone" onKeyPress="return MascaraTelefone(frmempresa.fone);"
                            onBlur="return ValidaTelefone(frmempresa.fone);" maxlength="14">
                    </div>
     <div class="col-lg-2 col-md-2">
                      <input class=form-control name="celular" id="celular" onKeyPress="return MascaraCelular(frmempresa.celular);"
                            onBlur="return ValidaCelular(frmempresa.celular);" maxlength="15">
                    </div>
                    <div class="col-lg-3 col-md-3" id="msgemail">
                      <input class=form-control name="email" id="email" onblur="validacaoEmail(frmempresa.email)"  maxlength="60" >
                    </div>


                  <!-- 
                  <div class="col-lg-2 col-md-2">
                      <input class=form-control name="fone" id="fone" onKeyPress="return MascaraTelefone(frmempresa.fone);"
                            onBlur="return ValidaTelefone(frmempresa.fone);" maxlength="14">
                    </div>
     <div class="col-lg-2 col-md-2">
                      <input class=form-control name="celular" id="celular" onKeyPress="return MascaraCelular(frmempresa.celular);"
                            onBlur="return ValidaCelular(frmempresa.celular);" maxlength="15">
                    </div>
                    <div class="col-lg-3 col-md-3" id="msgemail">
                      <input class=form-control name="email" id="email" onblur="validacaoEmail(frmempresa.email)"  maxlength="60" >
                    </div>

                -->



                    </div> 
                    <button class="btnvalidar" type=submit>Próximo</button>
                   
                
                       </form></div></div>

 <div class="tab-pane fade" id="endereco">
   <p id="loadingCep"></p>
 <form  name="frmendereco" id="frmendereco" method="Post" action="enviarCadastros/cadEmpresa_EnderecoEnviarDados.php" onSubmit="return validaEndereco();">

            <div class="box-endereco" name="box-endereco" id="box-endereco">
 <a href="#" ><img class="btndeletarendereco" id="btndeletarendereco"  src="assets\img\deletar.png" alt="Excluir endereço" title="Excluir endereço"></a>
<!--
     <img class="btndeletarendereco" id="btndeletarendereco"  src="assets\img\deletar.png" alt="Excluir endereço" title="Excluir endereço">
       <a href="#this" > <img class="btndeletarendereco" id="btndeletarendereco"  src="assets\img\deletar.png" alt="Excluir endereço" title="Excluir endereço"></a>
-->
<!--
<script type="text/javascript">

 var div = "#box-endereco";
 $(document).ready(function(){

$("#btndeletarendereco").hide();
    });
 
    $("#btnaddendereco").click(function () {
   
         $("#btndeletarendereco").show();
 
novadiv = $("#box-endereco:last").clone();

  novadiv.insertAfter(".box-endereco-copia:last");
  this.reset("#box-endereco");
     
    });

$("#btndeletarendereco").click(function(){
   $("#box-endereco:last").remove();
    document.div.display = 'none';
});
</script>
-->
 <div class=form-group>
  <label style="color:#000;"> Endereço da Empresa</label>
  <hr>
                  <label class="col-sm-5 control-label">Tipo de Endereço:</label>
                    <label class="col-sm-5 control-label">CEP:</label>
              
       
                  <div class=row>
                    <div class="col-lg-5 col-md-5">
                        <select class=form-control name="tipoEndereco" id="tipoEndereco" >
                      
                          <option value=>
                          <option value=a>Comercial
                          <option value=b>Residencial
                          </optgroup>
                        </select>
                    </div> 
                    <!--
                      <input class=form-control type="text" id="cep" name ="cep" value="" maxlength="8">
                      <input class=form-control type="text" id="cep" name ="cep" value="" maxlength="9" onBlur="return ValidaCep(frmendereco.cep);" onKeyPress="return MascaraCep(frmendereco.cep);" >
                   <img class="btnpesquisacep" onclick="buscaEndereco()" src="assets\img\cep.png" title="Pesquisar CEP" alt="Pesquisar CEP">
                    -->

   <div class="col-lg-3 col-md-3">
                      <input class=form-control type="text" id="cep" name ="cep" value="" maxlength="8" onBlur="return ValidaCep(frmendereco.cep);" onKeyPress="return MascaraCep(frmendereco.cep);">
                    </div>
                    <div class="col-lg-1 col-md-1">

                   <img class="btnpesquisacep" onclick="buscaEndereco()" src="assets\img\cep.png" title="Pesquisar CEP" alt="Pesquisar CEP">
                    
                    </div>

                       </div>
   </div>
                  <div class=row>
    <label class="col-sm-2 control-label">Logradouro:</label>
     <label class="col-sm-8 control-label">Endereço:</label>
       <label class="col-sm-1 control-label">N°:</label>

                    <div class="col-lg-2 col-md-2">
                      <select class=form-control name="tipoLogradouro" id="tipoLogradouro">
                      
                          <option value=>
                          <option value=a>Rua
                          <option value=b>Avenida
                          </optgroup>
                       
                        </select>
                    </div>



 <div class=row>
                   <div class="col-lg-8 col-md-8">
                      <input class=form-control name="dsendereco" id="dsendereco" type="text">
                      <input type="hidden" name="identidade" id="identidade" >
                    </div> 

                     <div class="col-lg-1 col-md-1">
                      <input class=form-control name="nrendereco" id="nrendereco" type="text">
                  
 
</div>
                    </div>
                 
    </div> <br>

  <label class="col-sm-4 control-label">Bairro:</label>
   <label class="col-sm-5 control-label">Município:</label>
          <label class="col-sm-2 control-label">UF:</label>
                  <div class=row>
                   <div class="col-lg-4 col-md-4">
                      <input class=form-control name="bairro"  id="bairro" type="text">
                    </div>
                    <div class="col-lg-5 col-md-5">
                      <input class=form-control name="cidade"  id="cidade" type="text">
                    </div>
                    <div class="col-lg-2 col-md-2">
                   <select class=form-control name="uf" id="uf">
                          <option value=>
                          <option>Escolha o Estado</option>
                          <option value="AC">AC</option>
                          <option value="AL">AL</option>
                          <option value="AP">AP</option>
                          <option value="AM">AM</option>
                          <option value="BA">BA</option>
                          <option value="CE">CE</option>
                          <option value="DF">DF</option>
                          <option value="ES">ES</option>
                          <option value="GO">GO</option>
                          <option value="MA">MA</option>
                          <option value="MT">MT</option>
                          <option value="MS">MS</option>
                          <option value="MG">MG</option>
                          <option value="PA">PA</option>
                          <option value="PB">PB</option>
                          <option value="PR">PR</option>
                          <option value="PE">PE</option>
                          <option value="PI">PI</option>
                          <option value="RJ">RJ</option>
                          <option value="RN">RN</option>
                          <option value="RS">RS</option>
                          <option value="RO">RO</option>
                          <option value="RR">RR</option>
                          <option value="SC">SC</option>
                          <option value="SP">SP</option>
                          <option value="SE">SE</option>
                          <option value="TO">TO</option>
 </select>
                    </div>
     </div> <br>    
 <div class=row> <label class="col-sm-9 control-label">Complemento:</label>
                   <div class="col-lg-5 col-md-5">
                      <input class=form-control name="complemento" id="complemento" type="text">
                    </div> 
                 
    </div> 
    </div> 
       <br>
</form>

<div id="copia" class="copia">

  <div id="box-endereco-copia" class="box-endereco-copia" id="box-endereco-copia">
 
</div>

</div>
<button class="btnsalvartodos" type=submit>Salvar todos</button>

 <img class="btnaddendereco"  id="btnaddendereco" src="assets\img\add.png" alt="Adicionar Endereço" title="Adicionar Endereço" >
<img class="btnremoveendereco"  id="btnremoveendereco" src="assets\img\remove.png" alt="Remover Endereço" title="Remover Endereço" >

 <button class="btnsalvarendereco"  id="btnsalvarendereco" ><img src="assets\img\salvar.png" alt="Salvar/Alterar Endereço" title="Salvar/Alterar Endereço" >

</button>
<!--
<button class="btnsalvartodos" type=submit>Salvar todos</button>

 <button class="btnaddendereco"  id="btnaddendereco"><img src="assets\img\add.png" alt="Adicionar Endereço" title="Adicionar Endereço" >
</button>
 <button class="btnremoveendereco"  id="btnremoveendereco" ><img src="assets\img\remove.png" alt="Remover Endereço" title="Remover Endereço" >
</button>
 <button class="btnsalvarendereco"  id="btnsalvarendereco" ><img src="assets\img\salvar.png" alt="Salvar/Alterar Endereço" title="Salvar/Alterar Endereço" >
</button>
-->
<script type="text/javascript">
 
 $(document).ready(function(){

$(".btnremoveendereco").hide();
$(".btndeletarendereco").hide();
    });
 
    $("#btnaddendereco").click(function () {
       $(".btnremoveendereco").show();
         $(".btndeletarendereco").show();
 
novadiv = $(".box-endereco:last").clone();

  novadiv.insertAfter(".box-endereco-copia:last");
  this.reset(".box-endereco");

     
    });

$(".btnremoveendereco").click(function(){
    $("#box-endereco:last").remove();
});

$(".btnremoveendereco").click(function(){
    $("#box-endereco:last").remove();
});
</script>
</div>
<!--
<div class="tab-pane fade" id="contatos">
   <form action=# name="frmcontatos" id="frmcontatos">

            <div class="box-contatos" name="box-contatos" id="box-contatos">

             
 <div class=form-group>
 
                  <label class="col-sm-5 control-label">Tipo de Contato:</label>
                  <label class="col-sm-5 control-label">Nome:</label>
       
                  <div class=row>
                    <div class="col-lg-5 col-md-5">
                        <select class=form-control name="tipocontato" required>
                      
                          <option value=a>A
                          <option value=b>B
                          </optgroup>
                       
                        </select>

                    </div>
                    <div class="col-lg-6 col-md-6">
                      <input class=form-control name="nomecontatoempresa" required>
                    </div>
  
     </div>  </div>
<br>

  <label class="col-sm-5 control-label">Descrição do Contato:</label>
               
                  <div class=row>
                  
 
                  <div class="col-lg-9 col-md-9">
                    <textarea class="form-control limitTextarea" maxlength=70 rows=3 style="margin-left:0px;" name="desccontato"></textarea>
                  </div>
                    </div><div id="copia" class="copia">

  <div id="box-endereco-copia" class="box-endereco-copia" id="box-endereco-copia">
 


</div>

</div>


  
     </div> 
<button class="btnsalvartodos" type=submit>Salvar todos</button>

 <button class="btnaddendereco"  id="btnaddendereco"><img src="assets\img\add.png" alt="Adicionar Endereço" title="Adicionar Endereço" >

</button>
 <button class="btnremoveendereco"  id="btnremoveendereco" ><img src="assets\img\remove.png" alt="Remover Endereço" title="Remover Endereço" >

</button>
 <button class="btnsalvarendereco"  id="btnsalvarendereco" ><img src="assets\img\salvar.png" alt="Salvar/Alterar Endereço" title="Salvar/Alterar Endereço" >
</button>
</div>
-->
<div class="tab-pane fade" id="locais">

<form action="enviarCadastros/cadempresaEnviarDados.php?idtipo='EM'"  name="frmlocais" id="frmlocais" onSubmit="return validaLocal();">
            <div class="box-locais" name="box-locais" id="box-locais">
 <div class=form-group>
   <label style="color:#000;">Dados do Local</label>
  <hr>
    <div class=row>
  <label class="col-sm-11 control-label">Tipo de Local:</label>


                      <div class="col-lg-3 col-md-3">
                        <select class=form-control name="tipolocais" required>
                      
                          <option value=>
                          <option value=a>Execução
                          <option value=b>B
                          </optgroup>
                       
                        </select>

                    </div>      </div><br>

<div class=row>

                  <label class="col-sm-5 control-label"> Nome da Empresa:</label>

                  <label class="col-sm-6 control-label">Nome Fantasia:</label>
           
                
                    <div class="col-lg-4 col-md-4">
                      <input class=form-control  name="nomeempresa" required>

                    </div>
                      <div class="col-lg-1 col-md-1">
                        <a href="#" onclick="window.open('buscarEmpresa.php', 'Pagina', 'STATUS=NO, TOOLBAR=NO, LOCATION=NO, DIRECTORIES=NO, RESISABLE=NO, SCROLLBARS=YES, TOP=10, LEFT=10, WIDTH=900, HEIGHT=600');"><img class="btnpesquisaempresa" src="assets\img\search32.png" title="Pesquisar empresa" alt="Pesquisar empresa" ></a>
</div>
                    <div class="col-lg-3 col-md-3" >
                      <input class=form-control name="nomefantasia" required >
                    </div>
                     
                     

                  </div>
                </div>
                  
                  <label class="col-sm-3 control-label">CNPJ:</label>
                  <label class="col-sm-3 control-label">Inscrição Municipal:</label>
                  <label class="col-sm-5 control-label">Inscrição Estadual:</label>
                  <div class=row>
                    <div class="col-lg-3 col-md-3">
                      <input class=form-control name="" required >
                    </div>
                    <div class="col-lg-3 col-md-3">
                      <input class=form-control name="inscmunicipal" required>
                    </div>
                      <div class="col-lg-3 col-md-3">
                      <input class=form-control name="inscestadual" required>
                  

              </div>
                  </div>
<br>

                  <label class="col-sm-11 control-label">CNES:</label>
                 
                  <div class=row>
                   
                    <div class="col-lg-3 col-md-3">
                      <input class=form-control name="">
                    </div>                    </div>

<br>
 <div class=form-group>
 <label style="color:#000;">Endereço do Local</label>
  <hr>

  
                  <label class="col-sm-5 control-label">Tipo de Endereço:</label>
                    <label class="col-sm-5 control-label">CEP:</label>
              
       
                  <div class=row>
                    <div class="col-lg-5 col-md-5">
                        <select class=form-control name="tipoendereco required">
                      
                          <option value=>
                          <option value=a>Comercial
                          <option value=b>Residencial
                          </optgroup>
                       
                        </select>

                    </div> 


   <div class="col-lg-3 col-md-3">
                      <input class=form-control name="cepempresa" required>
                    </div>

                       </div>
   </div>
                  <div class=row>
    <label class="col-sm-2 control-label">Logradro:</label>
     <label class="col-sm-8 control-label">Endereço:</label>
       <label class="col-sm-1 control-label">N°:</label>

                    <div class="col-lg-2 col-md-2">
                      <select class=form-control name="logradouro required">
                      
                          <option value=>
                          <option value=a>Rua
                          <option value=b>Avenida
                          </optgroup>
                       
                        </select>
                    </div>



 <div class=row>
                   <div class="col-lg-8 col-md-8">
                      <input class=form-control name="enderecoempresa">
                    </div> 

                     <div class="col-lg-1 col-md-1">
                      <input class=form-control name="numeroempresa" required>
                  
 
</div>
                    </div>
                 
    </div> <br>

  <label class="col-sm-4 control-label">Bairro:</label>
   <label class="col-sm-5 control-label">Município:</label>
          <label class="col-sm-2 control-label">UF:</label>
                  <div class=row>
                   <div class="col-lg-4 col-md-4">
                      <input class=form-control name="bairroempresa" required>
                    </div>
                    <div class="col-lg-5 col-md-5">
                      <input class=form-control name="municipioempresa" required>
                    </div>
                    <div class="col-lg-2 col-md-2">
                    <select class=form-control name="" id="">
                          <option value=>
                          <option>Escolha o Estado</option>
                          <option value="AC">AC</option>
                          <option value="AL">AL</option>
                          <option value="AP">AP</option>
                          <option value="AM">AM</option>
                          <option value="BA">BA</option>
                          <option value="CE">CE</option>
                          <option value="DF">DF</option>
                          <option value="ES">ES</option>
                          <option value="GO">GO</option>
                          <option value="MA">MA</option>
                          <option value="MT">MT</option>
                          <option value="MS">MS</option>
                          <option value="MG">MG</option>
                          <option value="PA">PA</option>
                          <option value="PB">PB</option>
                          <option value="PR">PR</option>
                          <option value="PE">PE</option>
                          <option value="PI">PI</option>
                          <option value="RJ">RJ</option>
                          <option value="RN">RN</option>
                          <option value="RS">RS</option>
                          <option value="RO">RO</option>
                          <option value="RR">RR</option>
                          <option value="SC">SC</option>
                          <option value="SP">SP</option>
                          <option value="SE">SE</option>
                          <option value="TO">TO</option>
                     </select>
                    </div>
     </div> <br>    
 <div class=row> <label class="col-sm-9 control-label">Complemento:</label>
                   <div class="col-lg-5 col-md-5">
                      <input class=form-control name="complemento">
                    </div> 


                 
    </div> <br>

 <label style="color:#000;"> Contato do Local</label>
  <hr>
                  <label class="col-sm-3 control-label">Nome do Contato:</label>

                  <label class="col-sm-2 control-label">Telefone:</label>
          <label class="col-sm-2 control-label">Celular:</label>
                 <label class="col-sm-5 control-label">E-mail:</label>
      
       
                  <div class=row>
                   
                    <div class="col-lg-3 col-md-3">
                      <input class=form-control name="nomecontato" required>
                    </div>
                     <div class="col-lg-2 col-md-2">
                      <input class="form-control" name="fone" id="fone" onkeypress="return MascaraTelefone(frmlocais.fone);" onblur="return ValidaTelefone(frmlocais.fone);" maxlength="14" value="">
                      <!--
                      <input class="form-control" name="fone" id="fone" onKeyPress="return MascaraTelefone(frmlocais.fone);"
                            onBlur="return ValidaTelefone(frmlocais.fone);" maxlength="14">
                       -->
                    </div>
     <div class="col-lg-2 col-md-2">
                      <input class=form-control name="celular" required>
                      <!--
                      <input class=form-control name="celular" id="celular" onKeyPress="return MascaraCelular(frmlocais.celular);"
                            onBlur="return ValidaCelular(frmlocais.celular);" maxlength="15">
                      -->
                    </div>
                    <div class="col-lg-3 col-md-3">
                      <input class=form-control name="email" required>
                       <!--
                      <div class="col-lg-3 col-md-3" id="msgemail">
                      <input class="form-control" name="email" id="email" onblur="validacaoEmail(frmlocais.email)"  maxlength="60" >
                    </div> 
                    -->
                    </div>
                    </div> 

     <button class="btnvalidar" type=submit style="margin-left:450px;">Finalizar</button>
    </div> 
     

<div class="box-locais-copia">

</div>
</div>
</div>
<!--

 <button class="btnaddlocal"  id="btnaddlocal"><img src="assets\img\add.png" alt="Adicionar Local" title="Adicionar Local" >

</button>
 <button class="btnremovelocal"  id="btnremovelocal" ><img src="assets\img\remove.png" alt="Remover Local" title="Remover Local" >

</button>
 <button class="btnsalvarlocal"  id="btnsalvarcontato" ><img src="assets\img\salvar.png" alt="Salvar/Alterar Local" title="Salvar/Alterar Local" >

</button>-->


</div>

               </form>

         
<script src=assets/plugins/core/pace/pace.min.js></script>
<!-- Important javascript libs(put in all pages) -->
<script>window.jQuery || document.write('<script src="assets/js/libs/jquery-2.1.1.min.js">\x3C/script>')</script>
<script src=http://code.jquery.com/ui/1.10.4/jquery-ui.js></script>
<script>window.jQuery || document.write('<script src="assets/js/libs/jquery-ui-1.10.4.min.js">\x3C/script>')</script>
<!--[if lt IE 9]>
  <script type="text/javascript" src="assets/js/libs/excanvas.min.js"></script>
  <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <script type="text/javascript" src="assets/js/libs/respond.min.js"></script>
<![endif]-->
<script src=assets/js/pages/blank.js></script>
<script src=assets/js/pages/forms.js></script>
<!-- Google Analytics:  -->
