<!DOCTYPE html>
<html lang=en>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
<meta charset=utf-8>
<title>Form validation | sprFlat - Admin Template</title>
<!-- Mobile specific metas -->
<meta name=viewport content="width=device-width,initial-scale=1,maximum-scale=1">
<!-- Force IE9 to render in normal mode -->
<!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
<meta name=author content=SuggeElson>
<meta name=description content="sprFlat admin template - new premium responsive admin template. This template is designed to help you build the site administration without losing valuable time.Template contains all the important functions which must have one backend system.Build on great twitter boostrap framework">
<meta name=keywords content="admin, admin template, admin theme, responsive, responsive admin, responsive admin template, responsive theme, themeforest, 960 grid system, grid, grid theme, liquid, jquery, administration, administration template, administration theme, mobile, touch , responsive layout, boostrap, twitter boostrap">
<meta name=application-name content="sprFlat admin template">
<!-- Import google fonts - Heading first/ text second -->
<link rel=stylesheet type=text/css href="http://fonts.googleapis.com/css?family=Open+Sans:400,700|Droid+Sans:400,700">
<!--[if lt IE 9]>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400" rel="stylesheet" type="text/css" />
<link href="http://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet" type="text/css" />
<link href="http://fonts.googleapis.com/css?family=Droid+Sans:400" rel="stylesheet" type="text/css" />
<link href="http://fonts.googleapis.com/css?family=Droid+Sans:700" rel="stylesheet" type="text/css" />
<![endif]-->
<!-- Css files -->
<link rel=stylesheet href=assets/css/main.min.css>
<!-- Fav and touch icons -->
<link rel=apple-touch-icon-precomposed sizes=144x144 href=assets/img/ico/apple-touch-icon-144-precomposed.png>
<link rel=apple-touch-icon-precomposed sizes=114x114 href=assets/img/ico/apple-touch-icon-114-precomposed.png>
<link rel=apple-touch-icon-precomposed sizes=72x72 href=assets/img/ico/apple-touch-icon-72-precomposed.png>
<link rel=apple-touch-icon-precomposed href=assets/img/ico/apple-touch-icon-57-precomposed.png>
<link rel=icon href=assets/img/ico/favicon.ico type=image/png>
<!-- Windows8 touch icon ( http://www.buildmypinnedsite.com/ )-->
<meta name=msapplication-TileColor content=#3399cc>
<body>


<?php include ("elements/header.php"); ?>

<?php include ("elements/sidebar.php"); ?>

<!-- Start #right-sidebar -->
<div id=right-sidebar class=hide-sidebar>
  <!-- Start .sidebar-inner -->
  <div class=sidebar-inner>
    <div class="sidebar-panel mt0">
      <div class="sidebar-panel-content fullwidth pt0">
        <div class=chat-user-list>
          <form class="form-horizontal chat-search" role=form>
            <div class=form-group>
              <input class=form-control placeholder="Search for user...">
              <button type=submit><i class="ec-search s16"></i></button>
            </div>
            <!-- End .form-group  -->
          </form>
          <ul class="chat-ui bsAccordion">
            <li><a href=#>Favorites <span class="notification teal">4</span><i class=en-arrow-down5></i></a>
              <ul class=in>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/49.jpg alt=@chadengle>Chad Engle <span class=has-message><i class=im-pencil></i></span></a> <span class="status online"><i class=en-dot></i></span></li>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/54.jpg alt=@alagoon>Anthony Lagoon</a> <span class="status offline"><i class=en-dot></i></span></li>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/52.jpg alt=@koridhandy>Kory Handy</a> <span class=status><i class=en-dot></i></span></li>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/50.jpg alt=@divya>Divia Manyan</a> <span class=status><i class=en-dot></i></span></li>
              </ul>
            </li>
            <li><a href=#>Online <span class="notification green">3</span><i class=en-arrow-down5></i></a>
              <ul class=in>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/51.jpg alt=@kolage>Eric Hofman</a> <span class="status online"><i class=en-dot></i></span></li>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/55.jpg alt=@mikebeecham>Mike Beecham</a> <span class="status online"><i class=en-dot></i></span></li>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/53.jpg alt=@derekebradley>Darek Bradly</a> <span class="status online"><i class=en-dot></i></span></li>
              </ul>
            </li>
            <li><a href=#>Offline <span class="notification red">5</span><i class=en-arrow-down5></i></a>
              <ul>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/56.jpg alt=@laurengray>Lauren Grey</a> <span class="status offline"><i class=en-dot></i></span></li>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/49.jpg alt=@chadengle>Chad Engle</a> <span class="status offline"><i class=en-dot></i></span></li>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/58.jpg alt=@frankiefreesbie>Frankie Freesibie</a> <span class="status offline"><i class=en-dot></i></span></li>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/57.jpg alt=@joannefournier>Joane Fornier</a> <span class="status offline"><i class=en-dot></i></span></li>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/59.jpg alt=@aiiaiiaii>Alia Alien</a> <span class="status offline"><i class=en-dot></i></span></li>
              </ul>
            </li>
          </ul>
        </div>
        <div class=chat-box>
          <h5>Chad Engle</h5>
          <a id=close-user-chat href=# class="btn btn-xs btn-primary"><i class=en-arrow-left4></i></a>
          <ul class="chat-ui chat-messages">
            <li class=chat-user>
              <p class=avatar><img src=assets/img/avatars/49.jpg alt=@chadengle></p>
              <p class=chat-name>Chad Engle <span class=chat-time>15 seconds ago</span></p>
              <span class="status online"><i class=en-dot></i></span>
              <p class=chat-txt>Hello Sugge check out the last order.</p>
            </li>
            <li class=chat-me>
              <p class=avatar><img src=assets/img/avatars/48.jpg alt=SuggeElson></p>
              <p class=chat-name>SuggeElson <span class=chat-time>10 seconds ago</span></p>
              <span class="status online"><i class=en-dot></i></span>
              <p class=chat-txt>Ok i will check it out.</p>
            </li>
            <li class=chat-user>
              <p class=avatar><img src=assets/img/avatars/49.jpg alt=@chadengle></p>
              <p class=chat-name>Chad Engle <span class=chat-time>now</span></p>
              <span class="status online"><i class=en-dot></i></span>
              <p class=chat-txt>Thank you, have a nice day</p>
            </li>
          </ul>
          <div class=chat-write>
            <form action=# class=form-horizontal role=form>
              <div class=form-group>
                <textarea name=sendmsg id=sendMsg class="form-control elastic" rows=1></textarea>
                <a role=button class=btn id=attach_photo_btn><i class="fa-picture s20"></i></a>
                <input type=file name=attach_photo id=attach_photo>
              </div>
              <!-- End .form-group  -->
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End .sidebar-inner -->
</div>
<!-- End #right-sidebar -->
<!-- Start #content -->
<div id=content>
  <!-- Start .content-wrapper -->
  <div class=content-wrapper>
    <div class=row>
      <!-- Start .row -->
      <!-- Start .page-header -->
      <div class="col-lg-12 heading">
        <h1 class=page-header><i class=im-checkbox-checked></i> Form validation</h1>
        <!-- Start .bredcrumb -->
        <ul id=crumb class=breadcrumb>
        </ul>
        <!-- End .breadcrumb -->
        <!-- Start .option-buttons -->
        <div class=option-buttons>
          <div class=btn-toolbar role=toolbar>
            <div class="btn-group dropdown"><a class="btn dropdown-toggle" data-toggle=dropdown id=dropdownMenu1><i class="br-grid s24"></i></a>
              <div class="dropdown-menu pull-right" role=menu aria-labelledby=dropdownMenu1>
                <div class=option-dropdown>
                  <div class=shortcut-button><a href=#><i class=im-pie></i> <span>Earning Stats</span></a></div>
                  <div class=shortcut-button><a href=#><i class="ec-images color-dark"></i> <span>Gallery</span></a></div>
                  <div class=shortcut-button><a href=#><i class="en-light-bulb color-orange"></i> <span>Fresh ideas</span></a></div>
                  <div class=shortcut-button><a href=#><i class="ec-link color-blue"></i> <span>Links</span></a></div>
                  <div class=shortcut-button><a href=#><i class="ec-support color-red"></i> <span>Support</span></a></div>
                  <div class=shortcut-button><a href=#><i class="st-lock color-teal"></i> <span>Lock area</span></a></div>
                </div>
              </div>
            </div>
            <div class="btn-group dropdown"><a class="btn dropdown-toggle" data-toggle=dropdown id=dropdownMenu2><i class="ec-pencil s24"></i></a>
              <div class="dropdown-menu pull-right" role=menu aria-labelledby=dropdownMenu2>
                <div class=option-dropdown>
                  <div class=row>
                    <p class=col-lg-12>Quick post</p>
                    <form class=form-horizontal role=form>
                      <div class=form-group>
                        <div class=col-lg-12>
                          <input class=form-control placeholder="Enter title">
                        </div>
                      </div>
                      <!-- End .form-group  -->
                      <div class=form-group>
                        <div class=col-lg-12>
                          <textarea class="form-control wysiwyg" placeholder="Enter text"></textarea>
                        </div>
                      </div>
                      <!-- End .form-group  -->
                      <div class=form-group>
                        <div class=col-lg-12>
                          <input class="form-control tags1" placeholder="Enter tags">
                        </div>
                      </div>
                      <!-- End .form-group  -->
                      <div class=form-group>
                        <div class=col-lg-12>
                          <button class="btn btn-default btn-xs">Save Draft</button>
                          <button class="btn btn-success btn-xs pull-right">Publish</button>
                        </div>
                      </div>
                      <!-- End .form-group  -->
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <div class=btn-group><a class="btn dropdown-toggle" data-toggle=dropdown id=dropdownMenu3><i class="ec-help s24"></i></a>
              <div class="dropdown-menu pull-right" role=menu aria-labelledby=dropdownMenu3>
                <div class=option-dropdown>
                  <p>First time visitor ? <a href=# id=app-tour class="btn btn-success ml15">Take app tour</a></p>
                  <hr>
                  <p>Or check the <a href=# class="btn btn-danger ml15">FAQ</a></p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- End .option-buttons -->
      </div>
      <!-- End .page-header -->
    </div>
    <!-- End .row -->
    <!-- Page start here ( usual with .row ) -->
    <div class=outlet>
      <!-- Start .outlet -->
      <div class=row>
        <!-- Start .row -->
        <div class=col-lg-12>
          <!-- Start col-lg-12 -->
          <div class="panel panel-default toggle">
            <!-- Start .panel -->
            <div class=panel-heading>
              <h3 class=panel-title>Form fields</h3>
            </div>
            <div class=panel-body>
              <form class="form-horizontal group-border hover-stripped" role=form id=validate>
                <div class=form-group>
                  <label class="col-lg-2 control-label">Required field</label>
                  <div class=col-lg-10>
                    <input class="form-control required">
                  </div>
                </div>
                <div class=form-group>
                  <label class="col-lg-2 control-label">Email field</label>
                  <div class=col-lg-10>
                    <input id=email name=email type=email class=form-control placeholder="Type your email">
                  </div>
                </div>
                <!-- End .form-group  -->
                <div class=form-group>
                  <label class="col-lg-2 control-label">Required with min value 13</label>
                  <div class=col-lg-10>
                    <input class=form-control name=minval placeholder="">
                  </div>
                </div>
                <!-- End .form-group  -->
                <div class=form-group>
                  <label class="col-lg-2 control-label">Required with max value 13</label>
                  <div class=col-lg-10>
                    <input class=form-control name=maxval placeholder="">
                  </div>
                </div>
                <!-- End .form-group  -->
                <div class=form-group>
                  <label class="col-lg-2 control-label">Password field</label>
                  <div class=col-lg-10>
                    <input type=password class=form-control id=password name=password placeholder="Enter your password">
                    <input type=password class="form-control mt15" id=confirm_password name=confirm_passowrd placeholder="Repeat password">
                  </div>
                </div>
                <!-- End .form-group  -->
                <div class=form-group>
                  <label class="col-lg-2 control-label">Required with max lenght of 10</label>
                  <div class=col-lg-10>
                    <input class=form-control id=maxLenght name=maxLenght placeholder="">
                  </div>
                </div>
                <!-- End .form-group  -->
                <div class=form-group>
                  <label class="col-lg-2 control-label">Required range between 10-20 chars</label>
                  <div class=col-lg-10>
                    <input class=form-control id=rangelenght name=rangelenght placeholder="">
                  </div>
                </div>
                <!-- End .form-group  -->
                <div class=form-group>
                  <label class="col-lg-2 control-label">Required with url validaiton</label>
                  <div class=col-lg-10>
                    <input class=form-control id=url name=url placeholder="">
                  </div>
                </div>
                <!-- End .form-group  -->
                <div class=form-group>
                  <label class="col-lg-2 control-label">Required date</label>
                  <div class=col-lg-10>
                    <input class=form-control id=date name=date placeholder="">
                  </div>
                </div>
                <!-- End .form-group  -->
                <div class=form-group>
                  <label class="col-lg-2 control-label">Required number</label>
                  <div class=col-lg-10>
                    <input class=form-control id=number name=number placeholder="">
                  </div>
                </div>
                <!-- End .form-group  -->
                <div class=form-group>
                  <label class="col-lg-2 control-label">Required textarea</label>
                  <div class=col-lg-10>
                    <textarea class=form-control name=textarea id=textarea rows=4></textarea>
                  </div>
                </div>
                <!-- End .form-group  -->
                <div class=form-group>
                  <label class="col-lg-2 control-label">Required and accept credit card number</label>
                  <div class=col-lg-10>
                    <input class=form-control id=ccard name=ccard placeholder="">
                  </div>
                </div>
                <!-- End .form-group  -->
                <div class=form-group>
                  <label class="col-lg-2 control-label">Required checkbox</label>
                  <div class=col-lg-10>
                    <label class=checkbox>
                      <input type=checkbox name=agree id=agree value=option>
                      agree terms ?</label>
                  </div>
                </div>
                <!-- End .form-group  -->
                <div class=form-group>
                  <label class="col-lg-2 control-label">Required file upload</label>
                  <div class=col-lg-10>
                    <input type=file name=file id=file class=form-control>
                  </div>
                </div>
                <!-- End .form-group  -->
                <div class=form-group>
                  <label class="col-lg-2 control-label">Required select with filter</label>
                  <div class=col-lg-10>
                    <select class="form-control select2" name=select2 id=select2>
                      <option value="">Choose
                      <optgroup label="Alaskan/Hawaiian Time Zone">
                      <option value=AK>Alaska
                      <option value=HI>Hawaii
                      </optgroup>
                      <optgroup label="Pacific Time Zone">
                      <option value=CA>California
                      <option value=NV>Nevada
                      <option value=OR>Oregon
                      <option value=WA>Washington
                      </optgroup>
                      <optgroup label="Mountain Time Zone">
                      <option value=AZ>Arizona
                      <option value=CO>Colorado
                      <option value=ID>Idaho
                      <option value=MT>Montana
                      <option value=NE>Nebraska
                      <option value=NM>New Mexico
                      <option value=ND>North Dakota
                      <option value=UT>Utah
                      <option value=WY>Wyoming
                      </optgroup>
                      <optgroup label="Central Time Zone">
                      <option value=AL>Alabama
                      <option value=AR>Arkansas
                      <option value=IL>Illinois
                      <option value=IA>Iowa
                      <option value=KS>Kansas
                      <option value=KY>Kentucky
                      <option value=LA>Louisiana
                      <option value=MN>Minnesota
                      <option value=MS>Mississippi
                      <option value=MO>Missouri
                      <option value=OK>Oklahoma
                      <option value=SD>South Dakota
                      <option value=TX>Texas
                      <option value=TN>Tennessee
                      <option value=WI>Wisconsin
                      </optgroup>
                      <optgroup label="Eastern Time Zone">
                      <option value=CT>Connecticut
                      <option value=DE>Delaware
                      <option value=FL>Florida
                      <option value=GA>Georgia
                      <option value=IN>Indiana
                      <option value=ME>Maine
                      <option value=MD>Maryland
                      <option value=MA>Massachusetts
                      <option value=MI>Michigan
                      <option value=NH>New Hampshire
                      <option value=NJ>New Jersey
                      <option value=NY>New York
                      <option value=NC>North Carolina
                      <option value=OH>Ohio
                      <option value=PA>Pennsylvania
                      <option value=RI>Rhode Island
                      <option value=SC>South Carolina
                      <option value=VT>Vermont
                      <option value=VA>Virginia
                      <option value=WV>West Virginia
                      </optgroup>
                    </select>
                  </div>
                </div>
                <!-- End .form-group  -->
                <div class=form-group>
                  <div class=col-lg-offset-2>
                    <button class="btn btn-default ml15" type=submit>Test validation</button>
                  </div>
                </div>
                <!-- End .form-group  -->
              </form>
            </div>
          </div>
          <!-- End .panel -->
        </div>
        <!-- End col-lg-12 -->
      </div>
      <!-- End .row -->
      <!-- Page End here -->
    </div>
    <!-- End .outlet -->
  </div>
  <!-- End .content-wrapper -->
  <div class=clearfix></div>
</div>
<!-- End #content -->
<!-- Javascripts -->
<!-- Load pace first -->
<script src=assets/plugins/core/pace/pace.min.js></script>
<!-- Important javascript libs(put in all pages) -->
<script>window.jQuery || document.write('<script src="assets/js/libs/jquery-2.1.1.min.js">\x3C/script>')</script>
<script src=http://code.jquery.com/ui/1.10.4/jquery-ui.js></script>
<script>window.jQuery || document.write('<script src="assets/js/libs/jquery-ui-1.10.4.min.js">\x3C/script>')</script>
<!--[if lt IE 9]>
  <script type="text/javascript" src="assets/js/libs/excanvas.min.js"></script>
  <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <script type="text/javascript" src="assets/js/libs/respond.min.js"></script>
<![endif]-->
<script src=assets/js/pages/form-validation.js></script>
<!-- Google Analytics:  -->
