<!DOCTYPE html>
<html lang=en>
<?php
session_start();

require_once('model/Logradouro.php');
require_once('enviarCadastros/PesquisarLogradouro.php');
require_once('model/Entidade.php');
require_once('model/Entidade_Juridica.php');
require_once('pesquisas/PesquisaConvenio.php');

$ent = new Entidade;
$entJur = new Entidade_Juridica;

$data = populacombologradouro();
$json_str = json_decode($data,true);
$itens = $json_str['GetPesquisarlogradouroResult'];

$conv = populacomboConvenio();
$json_conv = json_decode($conv,true);
$itens_conv = $json_conv['GetPesquisarConvenioResult'];

$tipoEnd = populacomboTipoEndereco();
$json_TipoEnd = json_decode($tipoEnd,true);
$itens_Tipoend = $json_TipoEnd['GetPesquisaTipoEnderecoResult'];
//$ent->set_Idtipo($_GET['idtipo']);

 if(isset($_SESSION['identidade']) != null){
    $identidade      = $_SESSION['identidade'];
  }else{
   
  }

  if(isset($_SESSION['idtipo']) != null){
    $idtipo      = $_SESSION['idtipo'];
   // $ent->set_Idtipo($_GET['idtipo']);
  }else{
   
  }

  ?>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<meta charset=utf-8>
<title>Cadastro de Médico | Área Administrativa</title>
<!-- Mobile specific metas -->
<meta name=viewport content="width=device-width,initial-scale=1,maximum-scale=1">
<!-- Force IE9 to render in normal mode -->
<!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
<meta name=author content=SuggeElson>
<meta name=description content="">
<meta name=keywords content="">
<!-- Import google fonts - Heading first/ text second -->
<link rel=stylesheet type=text/css href="http://fonts.googleapis.com/css?family=Open+Sans:400,700|Droid+Sans:400,700">
<!--[if lt IE 9]>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400" rel="stylesheet" type="text/css" />
<link href="http://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet" type="text/css" />
<link href="http://fonts.googleapis.com/css?family=Droid+Sans:400" rel="stylesheet" type="text/css" />
<link href="http://fonts.googleapis.com/css?family=Droid+Sans:700" rel="stylesheet" type="text/css" />
<![endif]-->
<!-- Css files -->
<link rel=stylesheet href='assets/css/main.min.css'>

<link rel=stylesheet href='assets/css/bootstrap.css'>
<!-- Fav and touch icons -->
<link rel='apple-touch-icon-precomposed' sizes='144x144' href='assets/img/ico/apple-touch-icon-144-precomposed.png'>
<link rel='apple-touch-icon-precomposed' sizes='114x114' href='assets/img/ico/apple-touch-icon-114-precomposed.png'>
<link rel='apple-touch-icon-precomposed' sizes='72x72' href='assets/img/ico/apple-touch-icon-72-precomposed.png'>
<link rel='apple-touch-icon-precomposed' href='assets/img/ico/apple-touch-icon-57-precomposed.png'>
<link rel='icon' href='assets/img/ico/favicon.ico' type='image/png'>
<!-- Windows8 touch icon ( http://www.buildmypinnedsite.com/ )-->
<meta name='msapplication-TileColor' content='#3399cc'>
<script language="JavaScript" type="text/javascript" src="assets/js/validacoes/validaempresa.js" ></script>  
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script language="JavaScript" type="text/javascript" src="assets/js/jquery-2.1.1.js" ></script>

<script language="JavaScript">
function getEndereco(cep) {

    if($.trim(cep) != ""){
        $("#loadingCep").html('Pesquisando...');
 
        $.getJSON("pesquisas/pesquisarcep.php",{cep:cep}, function(resultadoCEP){  
         if ($.isEmptyObject(resultadoCEP["GetPesquisarEnderecoResult"])) {
            alert('Número de cep não encontrado.');

         }else{
           $.each(resultadoCEP["GetPesquisarEnderecoResult"], function(key, val) {
                $("#dsendereco").val(unescape(val.dsendereco));
                $("#bairro").val(unescape(val.bairro));
                $("#cidade").val(unescape(val.cidade));
                document.getElementById("tipoLogradouro").value = val.idlogradouro;
                document.getElementById("idendereco").value = val.idendereco;
                document.getElementById("uf").value = val.uf;
           
           });
         }
            
       });         
         
    }else{
        $("#loadingCep").html('Informe o CEP');
    }
}
function buscaEndereco(){
  if($("#cep").val() != ""){
    //alert($("#cep").val());
    getEndereco($("#cep").val());
 
  }else{
        alert("Favor preencher o campo cep");
  }
}

</script>

<!--script para trazer o cep na guia locais de execução -->
<script language="JavaScript">
function getEnderecol(cepl) {

    if($.trim(cepl) != ""){
        $("#loadingCep").html('Pesquisando...');
 
        $.getJSON("pesquisas/pesquisarcep.php",{cep:cepl}, function(resultadoCEP){  
         if ($.isEmptyObject(resultadoCEP["GetPesquisarEnderecoResult"])) {
            alert('Número de cep não encontrado.');

         }else{
           $.each(resultadoCEP["GetPesquisarEnderecoResult"], function(key, val) {  

                $("#dsenderecol").val(unescape(val.dsendereco));
                $("#bairrol").val(unescape(val.bairro));
                $("#cidadel").val(unescape(val.cidade));
                document.getElementById("tipoLogradourol").value = val.idlogradouro;
                document.getElementById("idenderecol").value = val.idendereco;
                document.getElementById("ufl").value = val.uf;
           
           });
         }
            
       });         
         
    }else{
        $("#loadingCep").html('Informe o CEP');
    }
}
function buscaEnderecolocal(){
  if($("#cepl").val() != ""){
    //alert($("#cep").val());
   
    getEnderecol($("#cepl").val());
 
  }else{
        alert("Favor preencher o campo cep");
  }
}

</script>
</head>
<body>
<?php include ("elements/header.php"); ?>

<?php include ("elements/sidebar.php"); ?>

<?php include ("elements/rightsidebar.php"); ?>
<!-- Start #content -->
<div id='content'>
   <div class='content-wrapper'>
   
    <div class='row'>
      <!-- Start .row -->
      <!-- Start .page-header -->
        <div class='col-lg-8 heading'>
        <h1 class='page-header'>
          <img src="assets\img\cad_empresa_img.png"> Cadastro de Empresas</h1></div>
             <div class='col-lg-4 heading'>
        <label class="nomeunidade">Conectado na Unidade: <font style="color:MediumSeaGreen;">@nomedaunidade</label></font> </div>
    </div>
  </div>
<div class='tabs'>
 
            <ul id='myTab2' class="nav nav-tabs nav-justified">
              <li><a href='#empresa' data-toggle='tab'>Empresa</a></li>
              <li><a href='#endereco' data-toggle='tab'>Endereço</a></li>

           <!--   <li><a href=#contatos data-toggle=tab>Contatos</a></li>-->
              <li><a href='#locais' data-toggle='tab'>Locais</a><span class="notification">4</span></li>
          
            </ul>
            <div id='myTabContent2' class='tab-content'>
         
                <div class="tab-pane fade active in" id="empresa">
                  <div class="box-empresa" name="box-empresa">
<form  name="frmempresa" method="Post" id="validate"  action="enviarCadastros/EnviarEmpresa.php?idtipo=EM" onSubmit="return valida();">

 <div class='form-group'>
 <label style="color:#000;"> Dados da Empresa</label>
  <hr>
                  <label class="col-sm-5 control-label"> Nome da Empresa:</label>

                  <label class="col-sm-6 control-label">Nome Fantasia:</label>
           
                  <div class='row'>
                    <div class="col-lg-4 col-md-4">
                    <input class=form-control  name="nome" id="nome" value="<?php echo $ent->get_Nome(); ?>">

                    </div>
                                 <div class="col-lg-1 col-md-1">                             

<img class="btnpesquisaempresa" onclick="setIdtipo('EM')" data-toggle='modal' data-target='#myModal' src="assets\img\search32.png" title="Pesquisar empresa" alt="Pesquisar empresa">

                                 </div>
                    <div class="col-lg-3 col-md-3" >
                      <input class=form-control name="nomefantasia" id="nomefantasia" value="<?php echo $entJur->get_Nomefantasia(); ?>">
                    </div>
                     
                        </div>                

                  </div>               
                  
                  <label class="col-sm-3 control-label">CNPJ:</label>
                  <label class="col-sm-3 control-label">Inscrição Municipal:</label>
                  <label class="col-sm-5 control-label">Inscrição Estadual:</label>
                  <div class='row'>
                    <div class="col-lg-3 col-md-3">
 <input class=form-control name="cnpj" id="cnpj" onKeyPress="return MascaraCNPJ(frmempresa.cnpj);" 
maxlength="18" onBlur="return ValidarCNPJ(frmempresa.cnpj);" value="<?php echo $entJur->get_Cnpj(); ?>">                    </div>
                    <div class="col-lg-3 col-md-3">
                      <input class=form-control name="inscr_municipal" id="inscr_municipal" value="<?php echo $entJur->get_Inscr_municipal(); ?>">
                    </div>
                      <div class="col-lg-3 col-md-3">
                      <input class=form-control name="inscr_estadual" id="inscr_estadual" value="<?php echo $entJur->get_Inscr_estadual(); ?>">
                      </div><Br>
                 </div>
         
                  <label class="col-sm-11 control-label">CNES:</label>
                 
                  <div class=row>
                   
                    <div class="col-lg-3 col-md-3">
                      <input class=form-control name="cnes" id="cnes" value="<?php echo $entJur->get_Cnes(); ?>">
                    </div>                    
        </div>
<br>
 <label style="color:#000;"> Contato da Empresa</label>
  <hr>
                  <label class="col-sm-3 control-label">Nome do Contato:</label>

                  <label class="col-sm-2 control-label">Telefone:</label>
          <label class="col-sm-2 control-label">Celular:</label>
                 <label class="col-sm-5 control-label">E-mail:</label>      
       
                  <div class=row>
                   
                    <div class="col-lg-3 col-md-3">
                      <input class=form-control name="nomecontato" id="nomecontato" value="<?php echo $ent->get_Nomecontato(); ?>">
                    </div>
                     <div class="col-lg-2 col-md-2">
                      <input class=form-control name="fone" id="fone" onKeyPress="return MascaraTelefone(frmempresa.fone);"  onBlur="return ValidaTelefone(frmempresa.fone);" maxlength="14" value="<?php echo $ent->get_Fone(); ?>">
                    </div>
          <div class="col-lg-2 col-md-2">
                      <input class=form-control name="celular" id="celular" onKeyPress="return MascaraCelular(frmempresa.celular);"
                            onBlur="return ValidaCelular(frmempresa.celular);" maxlength="15" value="<?php echo $ent->get_Celular(); ?>">
                    </div>
                    <div class="col-lg-3 col-md-3" id="msgemail">
                      <input class=form-control name="email" id="email" onblur="validacaoEmail(frmempresa.email)"  maxlength="60" value="<?php echo $ent->get_Email(); ?>">
                    </div>
                    </div> 
           </div>
                    <button class="btnvalidar" type='submit'>Próximo</button>                   
                
                     </form>
            
             </div>
          
<div class="tab-pane fade" id="endereco">
 <form  name="frmendereco" id="frmendereco" method="Post" action="enviarCadastros/EnviarEnderecoEmp.php" onSubmit="return validaEndereco();">
  <input type="hidden" name="identidade" id="identidade" value="<?php echo $identidade; ?>">
<input type="hidden" name="idendereco" id="idendereco" value="<?php  ?>">

<div class="box-endereco" name="box-endereco" id="box-endereco">
  <a href="#" ><img class="btndeletarendereco" id="btndeletarendereco"  src="assets\img\deletar.png" alt="Excluir endereço" title="Excluir endereço"></a>
<!--
     <img class="btndeletarendereco" id="btndeletarendereco"  src="assets\img\deletar.png" alt="Excluir endereço" title="Excluir endereço">
       <a href="#this" > <img class="btndeletarendereco" id="btndeletarendereco"  src="assets\img\deletar.png" alt="Excluir endereço" title="Excluir endereço"></a>
-->

<script type="text/javascript">

 var div = "#box-endereco";
 $(document).ready(function(){

$("#btndeletarendereco").hide();
    });
 
    $("#btnaddendereco").click(function(){
   
         $("#btndeletarendereco").show();
 
novadiv = $("#box-endereco:last").clone();

  novadiv.insertAfter(".box-endereco-copia:last");
  this.reset("#box-endereco");
     
    });

$("#btndeletarendereco").click(function(){
   $("#box-endereco:last").remove();
    document.div.display = 'none';
});
//var element = document.getElementById("btndeletarendereco");
//element.parentNode.removeChild(element);

</script>
      
      
 <div class='form-group'>
  
  <label style="color:#000;"> Endereço da Empresa</label>
  
  <hr>
                  <label class="col-sm-5 control-label">Tipo de Endereço:</label>
                    <label class="col-sm-5 control-label">CEP:</label>              
       
                  <div class=row>
                    <div class="col-lg-5 col-md-5">
                        <select class=form-control name="tipoEndereco" id="tipoEndereco" >
                      <option value=></option>
                                                
                          <?php 
                      foreach ($itens_Tipoend as $e) 
                        { //echo $e."<br>";?>
                       <option value=<?php echo $e['idtipoendereco']; ?> ><?php echo $e['dstipoendereco']; ?></option>
                      <?php } ?> 
                       
                        </select>

                    </div> 

          <div class="col-lg-3 col-md-3">
                      <input class=form-control type="text" id="cep" name ="cep" value="" maxlength="8" onBlur="return ValidaCep(frmendereco.cep);" onKeyPress="return MascaraCep(frmendereco.cep);">
                    </div>
                    <div class="col-lg-1 col-md-1">

                   <img class="btnpesquisacep" onclick="buscaEndereco()" src="assets\img\cep.png" title="Pesquisar CEP" alt="Pesquisar CEP">
                    
                    </div>

                       </div>
 
                  <div class=row>
    <label class="col-sm-2 control-label">Logradouro:</label>
     <label class="col-sm-8 control-label">Endereço:</label>
       <label class="col-sm-1 control-label">N°:</label>

                    <div class="col-lg-2 col-md-2">
                       <select class=form-control name="tipoLogradouro" id="tipoLogradouro" >
           <option value=></option>
            <?php 
            foreach ($itens as $e) 
            { ?>
            <option value=<?php echo $e['idlogradouro']; ?> ><?php echo $e['dslogradouro']; ?></option>
            <?php } ?>
            </select>
                    </div>  

 <div class=row>
                   <div class="col-lg-8 col-md-8">
                      <input class=form-control name="dsendereco" id="dsendereco" type="text">
                    </div> 

                     <div class="col-lg-1 col-md-1">
                      <input class=form-control name="nrendereco" id="nrendereco" type="text">
                   
</div>
                    </div>
                 </div>
     <br>

  <label class="col-sm-4 control-label">Bairro:</label>
   <label class="col-sm-5 control-label">Município:</label>
          <label class="col-sm-2 control-label">UF:</label>
                  <div class=row>
                   <div class="col-lg-4 col-md-4">
                      <input class=form-control name="bairro"  id="bairro" type="text">
                    </div>
                    <div class="col-lg-5 col-md-5">
                      <input class=form-control name="cidade"  id="cidade" type="text">
                    </div>
                    <div class="col-lg-2 col-md-2">
                    <select class=form-control name="uf" id="uf">
                          <option value=>
                          <option>Escolha o Estado</option>
                          <option value="AC">AC</option>
                          <option value="AL">AL</option>
                          <option value="AP">AP</option>
                          <option value="AM">AM</option>
                          <option value="BA">BA</option>
                          <option value="CE">CE</option>
                          <option value="DF">DF</option>
                          <option value="ES">ES</option>
                          <option value="GO">GO</option>
                          <option value="MA">MA</option>
                          <option value="MT">MT</option>
                          <option value="MS">MS</option>
                          <option value="MG">MG</option>
                          <option value="PA">PA</option>
                          <option value="PB">PB</option>
                          <option value="PR">PR</option>
                          <option value="PE">PE</option>
                          <option value="PI">PI</option>
                          <option value="RJ">RJ</option>
                          <option value="RN">RN</option>
                          <option value="RS">RS</option>
                          <option value="RO">RO</option>
                          <option value="RR">RR</option>
                          <option value="SC">SC</option>
                          <option value="SP">SP</option>
                          <option value="SE">SE</option>
                          <option value="TO">TO</option>
 </select>
                    </div>
     </div> <br>    
 <div class=row> <label class="col-sm-9 control-label">Complemento:</label>
                   <div class="col-lg-5 col-md-5">
                      <input class=form-control name="complemento" id="complemento" type="text">
                    </div> 
</div>
</div> 
 <br>  

 </div>
          
<div id="copia" class="copia">

  <div id="box-endereco-copia" class="box-endereco-copia" id="box-endereco-copia">

</div>

</div>
    
<img class="btnaddendereco"  id="btnaddendereco" src="assets\img\add.png" alt="Adicionar Endereço" title="Adicionar Endereço" >
<img class="btnremoveendereco"  id="btnremoveendereco" src="assets\img\remove.png" alt="Remover Endereço" title="Remover Endereço" >

 <button class="btnsalvarendereco"  id="btnsalvarendereco" ><img src="assets\img\salvar.png" alt="Salvar/Alterar Endereço" title="Salvar/Alterar Endereço" >

</button>
<button class="btnsalvartodos" type="submit">Salvar todos</button>
<script type="text/javascript">
 
 $(document).ready(function(){

$(".btnremoveendereco").hide();
$(".btndeletarendereco").hide();
    });
 
    $("#btnaddendereco").click(function () {
       $(".btnremoveendereco").show();
         $(".btndeletarendereco").show();
 
novadiv = $(".box-endereco:last").clone();

  novadiv.insertAfter(".box-endereco-copia:last");
  this.reset(".box-endereco");
     
    });

$(".btnremoveendereco").click(function(){
    $("#box-endereco:last").remove();
});

$(".btnremoveendereco").click(function(){
    $("#box-endereco:last").remove();
});
</script>
  </form> 
</div>

<div class="tab-pane fade" id="locais">

<form action="enviarCadastros/EnviarLocalEmp.php" name="frmlocais" method="Post" id="validate" onSubmit="return validaLocal();">
   <input type="hidden" name="identidade" id="identidade" value="<?php echo $identidade; ?>">
   <input type="hidden" name="idvinculol" id="idvinculol" value="">
 
           <input type="hidden" name="idenderecol" id="idenderecol" value=" ">
            <div class="box-locais" name="box-locais" id="box-locais">
             
 <div class=form-group>
   <label style="color:#000;">Dados do Local</label>
  <hr>
    <div class=row>
  <label class="col-sm-3 control-label">Tipo de Local:</label>
    <label class="col-sm-4 control-label">Convênio:</label>
     <label class="col-sm-3 control-label">Código Operadora:</label>

                      <div class="col-lg-3 col-md-3">
                         <select class=form-control name="tipolocal" id="tipolocal" >
                      
                          <option value=>
                          <option value=LE>Execução
                          <option value=LC>Solicitação
                          </optgroup>
                       
                        </select>

                    </div> 

                        <div class="col-lg-4 col-md-4">
                        <select class=form-control name="tipoconvenio" id="tipoconvenio">
                         <span class=help-block>Convênio</span></div>
                                                              
            <option value=>
            <?php 
            foreach ($itens_conv as $e) 
            { ?>
            <option value=<?php echo $e['identidade']; ?> ><?php echo $e['nome']; ?></option>
            <?php } ?>
            </select>
            
                    </div>  
                       <div class="col-lg-2 col-md-2">                                               
                         <input class='form-control' name="cod_operadora" id="cod_operadora">

                        </div> 

                        </div><br>

                  <div class=row>

                  <label class="col-sm-5 control-label">Nome da Empresa:</label>

                  <label class="col-sm-6 control-label">Nome Fantasia:</label>
                
                    <div class="col-lg-4 col-md-4">
                      <input class=form-control  name="nomel" id="nomel" >

                    </div>
                                 <div class="col-lg-1 col-md-1">
                                                               
<img class="btnpesquisaempresa" onclick="setIdtipo('LC')" data-toggle='modal' data-target='#myModal' src="assets\img\search32.png" title="Pesquisar empresa" alt="Pesquisar empresa">
                                </div>

                    <div class="col-lg-3 col-md-3" >
                      <input class=form-control name="nomefantasial" id="nomefantasial" >
                
                  </div>   </div>
                  
                  <label class="col-sm-3 control-label">CNPJ:</label>
                  <label class="col-sm-3 control-label">Inscrição Municipal:</label>
                  <label class="col-sm-5 control-label">Inscrição Estadual:</label>
                  <div class=row>
                    <div class="col-lg-3 col-md-3">
                      <input class=form-control name="cnpjl" id="cnpjl" onKeyPress="return MascaraCNPJ(frmlocais.cnpjl);" onBlur="return ValidarCNPJ(frmlocais.cnpjl);" maxlength="18" >
                    </div>
                    <div class="col-lg-3 col-md-3">
                      <input class=form-control name="inscr_municipall" id="inscr_municipall" >
                    </div>
                      <div class="col-lg-3 col-md-3">
                      <input class=form-control name="inscr_estaduall" id="inscr_estaduall" >
                  <br>

              </div>
                  </div>

                  <label class="col-sm-11 control-label">CNES:</label>
                 
                  <div class=row>
                   
                    <div class="col-lg-3 col-md-3">
                      <input class=form-control name="cnesl" id="cnesl" >
                    </div>                    </div>
</div>
<br>
 <div class=form-group>
 <label style="color:#000;">Endereço do Local</label>
  <hr>
  
                  <label class="col-sm-5 control-label">Tipo de Endereço:</label>
                    <label class="col-sm-5 control-label">CEP:</label>     
                     <div class=row>
                    <div class="col-lg-5 col-md-5">
                        <select class=form-control name="tipoenderecol" id="tipoenderecol" >
                      <option value=></option>
                          <?php 
                      foreach ($itens_Tipoend as $e) 
                        { //echo $e."<br>";?>
                       <option value=<?php echo $e['idtipoendereco']; ?> ><?php echo $e['dstipoendereco']; ?></option>
                      <?php } ?> 
                       
                        </select>

                    </div>           
                  
   <div class="col-lg-3 col-md-3">
                      <input class=form-control type="text" id="cepl" name="cepl" value="" maxlength="8" onBlur="return ValidaCep(frmlocais.cepl);" onKeyPress="return MascaraCep(frmlocais.cepl);">
                    </div>
                    <div class="col-lg-1 col-md-1">

                   <img class="btnpesquisacep" onclick="buscaEnderecolocal()" src="assets\img\cep.png" title="Pesquisar CEP" alt="Pesquisar CEP">
                    
                    </div>

                       </div>
 
                  <div class=row>
    <label class="col-sm-2 control-label">Logradouro:</label>
     <label class="col-sm-8 control-label">Endereço:</label>
       <label class="col-sm-1 control-label">N°:</label>

                    <div class="col-lg-2 col-md-2">
                       <select class=form-control name="tipoLogradourol" id="tipoLogradourol" >
                      
                          <option value=></option>
                          <?php 
                      foreach ($itens as $e) 
                        { //echo $e."<br>";?>
                       <option value=<?php echo $e['idlogradouro']; ?> ><?php echo $e['dslogradouro']; ?></option>
                      <?php } ?>                       
                        </select>
                    </div>

                   <div class=row>
                   <div class="col-lg-8 col-md-8">
                      <input class=form-control name="dsenderecol" id="dsenderecol" type="text">
                    </div> 

                     <div class="col-lg-1 col-md-1">
                      <input class=form-control name="nrenderecol" id="nrenderecol" type="text" >
 
                     </div>
                    </div>
                 
    </div> <br>

  <label class="col-sm-4 control-label">Bairro:</label>
   <label class="col-sm-5 control-label">Município:</label>
          <label class="col-sm-2 control-label">UF:</label>
                  <div class=row>
                   <div class="col-lg-4 col-md-4">
                      <input class=form-control name="bairrol"  id="bairrol" type="text">
                    </div>
                    <div class="col-lg-5 col-md-5">
                      <input class=form-control name="cidadel" id="cidadel" type="text" >
                    </div>
                    <div class="col-lg-2 col-md-2">
                      <select class=form-control name="ufl" id="ufl">
                          <option value=>
                          <option>Escolha o Estado</option>
                          <option value="AC">AC</option>
                          <option value="AL">AL</option>
                          <option value="AP">AP</option>
                          <option value="AM">AM</option>
                          <option value="BA">BA</option>
                          <option value="CE">CE</option>
                          <option value="DF">DF</option>
                          <option value="ES">ES</option>
                          <option value="GO">GO</option>
                          <option value="MA">MA</option>
                          <option value="MT">MT</option>
                          <option value="MS">MS</option>
                          <option value="MG">MG</option>
                          <option value="PA">PA</option>
                          <option value="PB">PB</option>
                          <option value="PR">PR</option>
                          <option value="PE">PE</option>
                          <option value="PI">PI</option>
                          <option value="RJ">RJ</option>
                          <option value="RN">RN</option>
                          <option value="RS">RS</option>
                          <option value="RO">RO</option>
                          <option value="RR">RR</option>
                          <option value="SC">SC</option>
                          <option value="SP">SP</option>
                          <option value="SE">SE</option>
                          <option value="TO">TO</option>
                     </select>
                    </div>
     </div> <br> 
</div>
 <div class=form-group>  
 <div class=row> <label class="col-sm-9 control-label">Complemento:</label>
                   <div class="col-lg-5 col-md-5">
                      <input class=form-control name="complementol" id="complementol" >
                    </div> 
    </div> <br>

 <label style="color:#000;"> Contato do Local</label>
  <hr>
                  <label class="col-sm-3 control-label">Nome do Contato:</label>

                  <label class="col-sm-2 control-label">Telefone:</label>
          <label class="col-sm-2 control-label">Celular:</label>
                 <label class="col-sm-5 control-label">E-mail:</label>      
       
                  <div class=row>
                   
                    <div class="col-lg-3 col-md-3">
                      <input class=form-control name="nomecontatol" id="nomecontatol" >
                    </div>
                     <div class="col-lg-2 col-md-2">
                     <input class="form-control" name="fonel" id="fonel" onkeypress="return MascaraTelefone(frmlocais.fonel);" onblur="return ValidaTelefone(frmlocais.fonel);" maxlength="14" >
                    </div>
     <div class="col-lg-2 col-md-2">
                      <input class=form-control name="celularl" id="celularl" onKeyPress="return MascaraCelular(frmlocais.celularl);" onBlur="return ValidaCelular(frmlocais.celularl);" maxlength="15">
                    </div>
                    <div class="col-lg-3 col-md-3" id="msgemail">
                        <input class="form-control" name="emaill" id="emaill" onblur="validacaoEmail(frmlocais.emaill)"  maxlength="60" >
                    </div>
                    </div> 
</div>
     <button class="btnfinalizar" type=submit style="margin-left:450px;">Finalizar</button>
    </div> 
</form>

</div>
</div>
</div>

</div>

<script language="JavaScript">
  function PesquisaEmpresa() {
  Conteudo = $("#conteudo").val();
  Coluna =$("#coluna").val();
  if ($("#idtipo").val() == 'LC'){
    idtipo = $("#idtipomodal").val();
  }else{
  idtipo = $("#idtipo").val();
  }
  idvinculo = $("#idvinculol").val();
  if (idvinculo== ""){
    idvinculo = 0;
  }
  tabBody=document.getElementsByTagName("tbody").item(0);
  //var indexTR = 0;
  var tabela = document.getElementById('pesquisa');  
  var linhas = tabela.getElementsByTagName('tr');
  var indexTR = linhas.length;
 
  if(indexTR > 1 ){
    for (i = linhas.length-1; i>=1; i--){  
            //alert(linhas[i].innerHTML);  
            if (i != 0){  
                document.getElementById('pesquisa').deleteRow(i);  
            }  
        } 
  }
  
    if($.trim(Coluna) != "" && $.trim(Conteudo) != "" && $.trim(idtipo) != "") {
        $("#loadingEmpresa").html('Pesquisando...');
     
        $.getJSON("pesquisas/PesquisaEmpresa.php",{Coluna:Coluna,Conteudo:Conteudo,idtipo:idtipo,idvinculo:idvinculo}, function(resultadoColuna){  
    
         if ($.isEmptyObject(resultadoColuna["GetPesquisaEntidadeJuriCamposResult"])) {
            alert('Empresa não encontrada.');
          }else{
         
           empresas = resultadoColuna["GetPesquisaEntidadeJuriCamposResult"];
           empresas.forEach(function(entry) {
                //criando linha

                row=document.createElement("tr");
                //criando  colunas da tabela
                cell1 = document.createElement("td");
                cell2 = document.createElement("td");
                cell3 = document.createElement("td");
               //add links
                cell1.innerHTML = '<a onclick=retornaEmpresa(' +entry.identidade+')>'+entry.nome+'</a>';
                cell2.innerHTML = '<a onclick=retornaEmpresa(' +entry.identidade+')>'+entry.Entidade_Juridica.nomefantasia+'</a>';
                cell3.innerHTML = '<a onclick=retornaEmpresa(' +entry.identidade+')>'+entry.Entidade_Juridica.cnpj+'</a>';
                
                row.appendChild(cell1);
                row.appendChild(cell2);
                row.appendChild(cell3);
                tabBody.appendChild(row);
            });     
         };
       });   
    }else{
        $("#loadingEmpresa").html('Informe o tipo');
    }
}

function retornaEmpresa(identidade){
   
  if($.trim(identidade) != ""){
    $.getJSON("pesquisas/PesquisaEmpresaRetorno.php",{identidade:identidade}, function(resultadoColuna){  
         
         if ($.isEmptyObject(resultadoColuna["GetPesquisaEntidadeJuridicaResult"])) {
            alert('Empresa não encontrada.');
          }else{
         
           empresas = resultadoColuna["GetPesquisaEntidadeJuridicaResult"];
           empresas.forEach(function(entry) {
            if(entry.idtipo == "EM") {
               $("#idvinculol").val(unescape(entry.identidade));
               $("#nome").val(unescape(entry.nome));
               $("#nomefantasia").val(unescape(entry.Entidade_Juridica.nomefantasia));
               $("#cnpj").val(unescape(entry.Entidade_Juridica.cnpj));
               $("#inscr_municipal").val(unescape(entry.Entidade_Juridica.inscr_municipal));
               $("#inscr_estadual").val(unescape(entry.Entidade_Juridica.inscr_estadual));
               $("#cnes").val(unescape(entry.Entidade_Juridica.cnes));
               $("#nomecontato").val(unescape(entry.nomecontato));
               $("#fone").val(unescape(entry.fone));
               $("#celular").val(unescape(entry.celular));
               $("#email").val(unescape(entry.email));

               cont = 0;
               enderecos =  entry.EnderecoMovs;
               
               enderecos.forEach(function(entry2){
                cont += 1;
               
                if(cont == 1){
                    
                  $("#tipoEndereco").val(unescape(entry2.idtipoendereco));
                  $("#tipoLogradouro").val(unescape(entry2.Endereco.Logradouro.idlogradouro));
                  $("#cep").val(unescape(entry2.Endereco.cep));
                 
                   $("#dsendereco").val(unescape(entry2.Endereco.dsendereco));
                   $("#nrendereco").val(unescape(entry2.Numero));
                   $("#bairro").val(unescape(entry2.Endereco.bairro));
                   $("#cidade").val(unescape(entry2.Endereco.cidade));

                   $("#uf").val(unescape(entry2.Endereco.uf));
                   $("#complemento").val(unescape(entry2.Complemento));
                   
                 } else if(cont >1){
                   
                  $("#tipoEndereco").val(unescape(entry2.idtipoendereco));
                  $("#tipoLogradouro").val(unescape(entry2.Endereco.Logradouro.idlogradouro));
                  $("#cep").val(unescape(entry2.Endereco.cep));
                 
                   $("#dsendereco").val(unescape(entry2.Endereco.dsendereco));
                   $("#nrendereco").val(unescape(entry2.Numero));
                   $("#bairro").val(unescape(entry2.Endereco.bairro));
                   $("#cidade").val(unescape(entry2.Endereco.cidade));

                   $("#uf").val(unescape(entry2.Endereco.uf));
                   $("#complemento").val(unescape(entry2.Complemento));

                  $(".btnremoveendereco").show();
                  $(".btndeletarendereco").show();
 
                  novadiv = $(".box-endereco:last").clone();

                  novadiv.insertAfter(".box-endereco-copia:last");
                }
               });

             }else if(entry.idtipo== "LE" || entry.idtipo== "LC" ) {
                 
                 $("#tipolocal").val(unescape(entry.idtipo));
                 $("#nomel").val(unescape(entry.nome));
                 $("#nomefantasial").val(unescape(entry.Entidade_Juridica.nomefantasia));
                 $("#cnpjl").val(unescape(entry.Entidade_Juridica.cnpj));
                 $("#inscr_municipall").val(unescape(entry.Entidade_Juridica.inscr_municipal));
                 $("#inscr_estaduall").val(unescape(entry.Entidade_Juridica.inscr_estadual));
                 $("#cnesl").val(unescape(entry.Entidade_Juridica.cnes));        
                 //$("#tipoenderecol").val(unescape(entry.));

                enderecos =  entry.EnderecoMovs;
               
               enderecos.forEach(function(entry2){
                
                 $("#cepl").val(unescape(entry2.Endereco.cep));
                 $("#tipoLogradourol").val(unescape(entry2.Endereco.Logradouro.idlogradouro));
                 $("#dsenderecol").val(unescape(entry2.Endereco.dsendereco));
                 $("#nrenderecol").val(unescape(entry2.Numero));
                 $("#bairrol").val(unescape(entry2.Endereco.bairro));
                 $("#cidadel").val(unescape(entry2.Endereco.cidade));
                 $("#ufl").val(unescape(entry2.Endereco.uf));
                 $("#complementol").val(unescape(entry2.Complemento));
                 $("#tipoenderecol").val(unescape(entry2.idtipoendereco));
               });

                 $("#nomecontatol").val(unescape(entry.nomecontato));
                 $("#fonel").val(unescape(entry.fone));
                 $("#celularl").val(unescape(entry.celular));
                 $("#emaill").val(unescape(entry.email)); 
                locais =  entry.Entidade_LocalExecSolic;
               
               locais.forEach(function(loc){
                  $("#tipoconvenio").val(unescape(loc.identidade_operadora)); 
                  $("#cod_operadora").val(unescape(loc.codoperadora));
                });

             }

            });                    

         }
       });   
          $('#myModal').modal('hide');
  $("#conteudo").val("");
          //var indexTR = 0;
  var tabela = document.getElementById('pesquisa');  
  var linhas = tabela.getElementsByTagName('tr');
  var indexTR = linhas.length;
 
  if(indexTR > 1 ){
    for (i = linhas.length-1; i>=1; i--){  
            //alert(linhas[i].innerHTML);  
            if (i != 0){  
                document.getElementById('pesquisa').deleteRow(i);  
            }  
        } 
  }
    }
}
</script>
<script type="text/javascript">
  function setIdtipo(idtipo){
     if($.trim(idtipo) != ""){
  
       document.getElementById("idtipo").value = idtipo;
       topomodal=document.getElementById("topomodal");
       grupopesquisa = document.getElementById("grupopesquisa");
      
      // topomodal.getElementById("topomodal").empty();
      $("#linha").remove();
      $("#titulo").remove();
        if (idtipo == 'EM'){
     
        h3 = document.createElement("h3");
        h3.innerHTML= '<h3 class="modal-title" id="titulo"><center>Pesquisa de Empresa</h3></center>';
        topomodal.appendChild(h3);

        }else{
        
        $("#titulo").remove();
        h3 = document.createElement("h3");
        h3.innerHTML= '<h3 class="modal-title" id="titulo"><center>Pesquisa de Locais</h3></center>';
        topomodal.appendChild(h3);

        div = document.createElement("div");
        div.innerHTML = '<div id="linha"><label class="col-lg-4 col-md-4 col-sm-12 control-label">Tipo de Local:</label><div class="col-lg-7 col-md-7"><div class=row id="Linha"><select class=form-control id="idtipomodal" name="idtipomodal"><option value="LE">Execução</option><option value="LS">Solicitação</option></select></div></div></div></br>';
       
        grupopesquisa.appendChild(div);
      
        }
    }

  }
  
</script>

<div class="modal fade" id="myModal">

<div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" name="topomodal" id='topomodal'>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            
        </div>
        <div class="modal-body">
     <div id="box-pesquisa">
      <h5 class="text-center">Insira os filtros que deseja para realizar a pesquisa:</h5>
      <form name="frmpesquisa" id="frmpesquisa" method="Post" >
        <input type="hidden" name="idtipo" id="idtipo" >

          <div class="form-group">

          <div class=form-group id='grupopesquisa'>

          </div>
<div class=form-group>
                   <label class="col-lg-4 col-md-4 col-sm-12 control-label">Coluna de Pesquisa:</label>
                  <div class="col-lg-7 col-md-7">
                    <div class=row>
                      
                        <select class=form-control id="coluna" name="coluna">
                                                 
                          <option value="Nome">Nome</option>
                          <option value="NomeFantasia">Nome Fantasia</option>
                          <option value="CNPJ">CNPJ</option>
                                                  
                        </select>
                       
                    </div>
                  </div>
                </div>
                <!-- End .form-group  -->
          <div class="form-group">

                  <label class="col-lg-4 col-md-4 col-sm-12 control-label">Conteúdo:</label>
            <textarea name='conteudo' class="form-control" id="conteudo"></textarea>
          </div>
       
          <div class="form-group" align= "center">
                <button type="button" class="btn btn-primary">Limpar</button>
              <button type="button" onclick="PesquisaEmpresa()" class="btn btn-primary">Pesquisar</button>
            <div class="clearfix">
            </div>
          </div>
      </form>
    
        <div class="modal-footer">
                
        </div>
        
          <table id="pesquisa"class="table table-condensed">
             <thead>
                  <tr>
                     <th>Nome</th>
                     <th>Nome Fantasia</th>
                     <th>CNPJ</th>
                  </tr>
               </thead>
               <tbody>
                 
               </tbody>
            </table>

         </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
</div>

</div>
</div>

<!-- Javascripts -->
<!-- Load pace first -->
<script src='assets/plugins/core/pace/pace.min.js'></script>
<!-- Important javascript libs(put in all pages) -->
<script>window.jQuery || document.write('<script src="assets/js/libs/jquery-2.1.1.min.js">\x3C/script>')</script>
<script src='http://code.jquery.com/ui/1.10.4/jquery-ui.js'></script>
<script>window.jQuery || document.write('<script src="assets/js/libs/jquery-ui-1.10.4.min.js">\x3C/script>')</script>
<!--[if lt IE 9]>
  <script type="text/javascript" src="assets/js/libs/excanvas.min.js"></script>
  <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <script type="text/javascript" src="assets/js/libs/respond.min.js"></script>
<![endif]-->
<script src='assets/js/pages/dashboard.js'></script>

</body>
</html>