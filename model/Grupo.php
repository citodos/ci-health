<?php
class Grupo{
	var $idgrupo;
	var $idsistema;
	var $dsgrupo;


	public function set_Idgrupo($idgrupo){
		if(! empty($_POST) && is_numeric($idgrupo)){
		    if(isset($_POST['idgrupo'])){
		   		 $idgrupo= $_POST['idgrupo'];
		    }	
		}else{
		    $idgrupo= "";
		}
	}

	public function get_Idgrupo(){
		return $this->idgrupo;
	}

	public function set_Idsistema($idsistema){
		if(! empty($_POST) && is_numeric($idsistema)){
		    if(isset($_POST['idsistema'])){
		   		 $idsistema= $_POST['idsistema'];
		    }	
		}else{
		    $idsistema= "";
		}
	}

	public function get_Idsistema(){
		return $this->idsistema;
	}

	public function set_Dsgrupo($dsgrupo){
		if(! empty($_POST)){
		    if(isset($_POST['dsgrupo'])){
		   		 $dsgrupo= $_POST['dsgrupo'];
		    }	
		}else{
		    $dsgrupo="";
		}
	}

	public function get_Dsgrupo(){
		return $this->dsgrupo;
	}
}

?>