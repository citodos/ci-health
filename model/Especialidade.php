<?php
class Especialidade{
	var $idespecialidade;
	var $dsespecialidade;
	var $cbo;

	public function set_Idespecialidade($idespecialidade){
		if(! empty($_POST) && is_numeric($idespecialidade)){
		    if(isset($_POST['idespecialidade'])){
		   		 $idespecialidade= $_POST['idespecialidade'];
		    }	
		}else{
		    $idespecialidade= "";
		}
	}

	public function get_Idespecialidade(){
		return $this->idespecialidade;
	}

	public function set_Dsespecialidade($dsespecialidade){
		if(! empty($_POST)){
		    if(isset($_POST['dsespecialidade'])){
		   		 $dsespecialidade= $_POST['dsespecialidade'];
		    }	
		}else{
		    $dsespecialidade="";
		}
	}

	public function get_Dsespecialidade(){
		return $this->dsespecialidade;
	}

	public function set_Cbo($cbo){
		if(! empty($_POST) && is_numeric($cbo)){
		    if(isset($_POST['cbo'])){
		   		 $cbo= $_POST['cbo'];
		    }	
		}else{
		    $cbo= "";
		}
	}

	public function get_cbo(){
		return $this->cbo;
	}


}
?>