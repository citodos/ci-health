<?php
class Entidade_EquipeMov{
	var $idequipemov;
	var $identidade_equipe;
	var $identidade_profissional;


	public function set_Idequipemov($idequipemov){
		if(! empty($_POST) && is_numeric($idequipemov)){
		    if(isset($_POST['idequipemov'])){
		   		 $idequipemov= $_POST['idequipemov'];
		    }	
		}else{
		    $idequipemov= "";
		}
	}

	public function get_Idequipemov(){
		return $this->idequipemov;
	}

	public function set_Identidade_equipe($identidade_equipe){
		if(! empty($_POST) && is_numeric($identidade_equipe)){
		    if(isset($_POST['identidade_equipe'])){
		   		 $identidade_equipe= $_POST['identidade_equipe'];
		    }	
		}else{
		    $identidade_equipe= "";
		}
	}

	public function get_Identidade_equipe(){
		return $this->identidade_equipe;
	}

	public function set_Identidade_profissional($identidade_profissional){
		if(! empty($_POST) && is_numeric($identidade_profissional)){
		    if(isset($_POST['identidade_profissional'])){
		   		 $identidade_profissional= $_POST['identidade_profissional'];
		    }	
		}else{
		    $identidade_profissional= "";
		}
	}

	public function get_Identidade_profissional(){
		return $this->identidade_profissional;
	}

}

?>