<?php
class Menu{
	var $idmenu;
	var $dsmenu;
	var $coluna_linhaid;
	var $nivel;
	var $parentid;
	var $subordem;
	var $objeto;
	var $idsistema;


	public function set_Idmenu($idmenu){
		if(! empty($_POST) && is_numeric($idmenu)){
		    if(isset($_POST['idmenu'])){
		   		 $idmenu= $_POST['idmenu'];
		    }	
		}else{
		    $idmenu= "";
		}
	}

	public function get_Idmenu(){
		return $this->idmenu;
	}

	public function set_Dsmenu($dsmenu){
		if(! empty($_POST)){
		    if(isset($_POST['dsmenu'])){
		   		 $dsmenu= $_POST['dsmenu'];
		    }	
		}else{
		    $dsmenu="";
		}
	}

	public function get_Dsmenu(){
		return $this->dsmenu;
	}

	public function set_Coluna_linhaid($coluna_linhaid){
		if(! empty($_POST) && is_numeric($coluna_linhaid)){
		    if(isset($_POST['coluna_linhaid'])){
		   		 $coluna_linhaid= $_POST['coluna_linhaid'];
		    }	
		}else{
		    $coluna_linhaid= "";
		}
	}

	public function get_Coluna_linhaid(){
		return $this->coluna_linhaid;
	}

	public function set_Nivel($nivel){
		if(! empty($_POST) && is_numeric($nivel)){
		    if(isset($_POST['nivel'])){
		   		 $nivel= $_POST['nivel'];
		    }	
		}else{
		    $nivel= "";
		}
	}

	public function get_Nivel(){
		return $this->nivel;
	}

	public function set_Parentid($parentid){
			if(! empty($_POST) && is_numeric($parentid)){
			    if(isset($_POST['parentid'])){
			   		 $parentid= $_POST['parentid'];
				 }	
			}else{
			    $parentid= "";
		}
	}

	public function get_Parentid(){
		return $this->parentid;
	}

	public function set_Subordem($subordem){
			if(! empty($_POST) && is_numeric($subordem)){
			    if(isset($_POST['subordem'])){
			   		 $subordem= $_POST['subordem'];
				 }	
			}else{
			    $subordem= "";
		}
	}

	public function get_Subordem(){
		return $this->subordem;
	}

	public function set_Objeto($objeto){
		if(! empty($_POST)){
		    if(isset($_POST['objeto'])){
		   		 $objeto= $_POST['objeto'];
		    }	
		}else{
		    $objeto="";
		}
	}

	public function get_Objeto(){
		return $this->objeto;
	}

	public function set_Idsistema($idsistema){
		if(! empty($_POST) && is_numeric($idsistema)){
		    if(isset($_POST['idsistema'])){
		   		 $idsistema= $_POST['idsistema'];
		    }	
		}else{
		    $idsistema= "";
		}
	}

	public function get_Idsistema(){
		return $this->idsistema;
	}

}

?>