<?php
class Logradouro{
	var $idlogradouro;
	var $dslogradouro;

	public function set_Idlogradouro($idlogradouro){
		if(! empty($_POST) && is_numeric($idlogradouro)){
		    if(isset($idlogradouro)){
		   		 $idlogradouro= $idlogradouro;
		    }	
		}else{
		    $idlogradouro= "";
		}
	}

	public function get_Idlogradouro(){
		return $this->idlogradouro;
	}

	public function set_Dslogradouro($dslogradouro){
		if(! empty($_POST)){
		    if(isset($_POST['dslogradouro'])){
		   		 $dslogradouro= $_POST['dslogradouro'];
		    }	
		}else{
		    $dslogradouro="";
		}
	}

	public function get_Dslogradouro(){
		return $this->dslogradouro;
	}
}
?>