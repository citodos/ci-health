<?php
class Entidade_Profissional_Especialidade{
	var $idprofissionalespec;
	var $identidade;
	var $idespecialidade;
	var $dtinicio;
	var $dtfim;


	public function set_Idprofissionalespec($idprofissionalespec){
		if(! empty($_POST) && is_numeric($idprofissionalespec)){
		    if(isset($_POST['idprofissionalespec'])){
		   		 $idprofissionalespec= $_POST['idprofissionalespec'];
		    }	
		}else{
		    $idprofissionalespec= "";
		}
	}

	public function get_Idprofissionalespec(){
		return $this->idprofissionalespec;
	}

	public function set_Identidade($identidade){
		if(! empty($_POST) && is_numeric($identidade)){
		    if(isset($_POST['identidade'])){
		   		 $identidade= $_POST['identidade'];
		    }	
		}else{
		    $identidade= "";
		}
	}

	public function get_Identidade(){
		return $this->identidade;
	}

	public function set_Idespecialidade($idespecialidade){
		if(! empty($_POST) && is_numeric($idespecialidade)){
		    if(isset($_POST['idespecialidade'])){
		   		 $idespecialidade= $_POST['idespecialidade'];
		    }	
		}else{
		    $idespecialidade= "";
		}
	}

	public function get_Idespecialidade(){
		return $this->idespecialidade;
	}

	public function set_Dtinicio($dtinicio){
		if(! empty($_POST)){
		    if(isset($_POST['dtinicio'])){
		   		 $dtinicio= $_POST['dtinicio'];
		    }	
		}else{
		    $dtinicio="";
		}
	}

	public function get_Dtinicio(){
		return $this->dtinicio;
	}

	public function set_Dtfim($dtfim){
		if(! empty($_POST)){
		    if(isset($_POST['dtfim'])){
		   		 $dtfim= $_POST['dtfim'];
		    }	
		}else{
		    $dtfim="";
		}
	}

	public function get_Dtfim(){
		return $this->dtfim;
	}

	
}
?>