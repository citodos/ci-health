<?php

class Entidade{

	var $identidade;
	var $idtipo;
	var $nome;
	var $fone;
	var $email;
	var $celular;
	var $flg_status;
	var $identidade_vinculo;
	var $nomecontato;
	

	public function set_Identidade($identidade){
		 
		if( is_numeric($identidade)){
		    if(isset($identidade)){
		   		 $this->identidade= $identidade;

		    }	
		}else{
		    $this->identidade="conteúdo inválido";
		}
	}

	public function get_Identidade(){
		
		return $this->identidade;
		
	}

	public function set_Idtipo($idtipo){
		if(! empty($idtipo)){
		    if(isset($idtipo)){
		   		 $this->idtipo= $idtipo;
		    }	
		}else{
			//echo "errado";
		    $this->idtipo="erro no idtipo";
		    //echo $identidade."<br />";
		}
	}

	public function get_Idtipo(){
		
		return $this->idtipo;
		
	}

	public function set_Nome($nome){
		if(! empty($nome)){
		    if(isset($nome)){
		   		 $this->nome= $nome;
		    }	
		}else{
		    $this->nome="";
		}
	}

	public function get_Nome(){
		return $this->nome;
	}

	public function set_Fone($fone){
		if(! empty($fone)){
		    if(isset($fone)){
		   		 $this->fone= $fone;
		   		 
		    }	
		}else{
		    $this->fone="";
		}
	}

	public function get_Fone(){
		return $this->fone;
	}

	public function set_Email($email){
		if(! empty($email)){
		    if(isset($email)){
		   		 $this->email= $email;
		    }	
		}else{
		    $this->email="";
		}
	}

	public function get_Email(){
		return $this->email;
	}

	public function set_Celular($celular){
		if(! empty($celular)){
		    if(isset($celular)){
		   		 $this->celular= $celular;
		   		
		    }	
		}else{
		    $this->celular="";
		}
	}

	public function get_Celular(){
		return $this->celular;
	}

	public function set_Flg_status($flg_status){
		if(! empty($flg_status)){
		    if(isset($flg_status)){
		   		 $this->flg_status=$flg_status;
		    }	
		}else{
		    $this->flg_status="";
		}
	}

	public function get_Flg_status(){
		return $this->flg_status;
	}

	public function set_Identidade_vinculo($identidade_vinculo){
		
		if(! empty($identidade_vinculo) &&  is_numeric($identidade_vinculo)){
		    if(isset($identidade_vinculo)){
		   		 $this->identidade_vinculo=$identidade_vinculo;
		    }	
		}else{
		    $this->identidade_vinculo=0;
		}
	}

	public function get_Identidade_vinculo(){
		return $this->identidade_vinculo;
	}

	public function set_Nomecontato($nomecontato){
		if(! empty($nomecontato)){
		    if(isset($nomecontato)){
		   		 $this->nomecontato= $nomecontato;
		    }	
		}else{
		    $this->nomecontato="";
		}
	}

	public function get_Nomecontato(){
		return $this->nomecontato;
	}

}


?>