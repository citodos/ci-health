<?php

class AgendaAusencia{
	var $idagendaausencia;
	var $identidade;
	var $dtausencia;
	var $idespecialidade;
	var $horainicio;
	var $horafim;
	var $diasemana;


	public function set_Idagendaausencia($idagendaausencia){
		if(! empty($_POST) && is_numeric($idagendaausencia)){
		    if(isset($_POST['idagendaausencia'])){
		   		 $idagendaausencia= $_POST['idagendaausencia'];
		    }	
		}else{
		    $idagendaausencia="";
		}
	}

	public function get_Idagendaausencia(){
		return $this->idagendaausencia;
	}

	public function set_Identidade($identidade){
		if(! empty($_POST) && is_numeric($identidade)){
		    if(isset($_POST['identidade'])){
		   		 $identidade= $_POST['identidade'];
		    }	
		}else{
		    $identidade= "";
		}
	}

	public function get_Identidade(){
		return $this->identidade;
	}

	public function set_Dtausencia($dtausencia){
		if(! empty($_POST)){
		    if(isset($_POST['dtausencia'])){
		   		 $dtausencia= $_POST['dtausencia'];
		    }	
		}else{
		    $dtausencia="";
		}
	}

	public function get_Dtausencia(){
		return $this->dtausencia;
	}

	public function set_Idespecialidade($idespecialidade){
		if(! empty($_POST) && is_numeric($idespecialidade)){
		    if(isset($_POST['idespecialidade'])){
		   		 $idespecialidade= $_POST['idespecialidade'];
		    }	
		}else{
		    $idespecialidade= "";
		}
	}

	public function get_Idespecialidade(){
		return $this->idespecialidade;
	}

	public function set_Horainicio($horainicio){
		if(! empty($_POST)){
		    if(isset($_POST['horainicio'])){
		   		 $horainicio= $_POST['horainicio'];
		    }	
		}else{
		    $horainicio="";
		}
	}

	public function get_Horainicio(){
		return $this->horainicio;
	}

	public function set_Horafim($horafim){
		if(! empty($_POST)){
		    if(isset($_POST['horafim'])){
		   		 $horafim= $_POST['horafim'];
		    }	
		}else{
		    $horafim="";
		}
	}

	public function get_Horafim(){
		return $this->horafim;
	}

	public function set_Diasemana($diasemana){
		if(! empty($_POST) && is_numeric($diasemana)){
		    if(isset($_POST['diasemana'])){
		   		 $diasemana= $_POST['diasemana'];
		    }	
		}else{
		    $diasemana= "";
		}
	}

	public function get_Diasemana(){
		return $this->diasemana;
	}

}

?>