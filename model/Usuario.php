<?php
class Usuario{
	var $idusuario;	
	var $nome;
	var $senha;
	var $idgrupo;
	var $identidade;

	public function set_Idusuario($idusuario){
		if(! empty($_POST) && is_numeric($idusuario)){
		    if(isset($_POST['idusuario'])){
		   		 $idusuario= $_POST['idusuario'];
		    }	
		}else{
		    $idusuario= "";
		}
	}

	public function get_Idusuario(){
		return $this->idusuario;
	}

	public function set_Nome($nome){
		if(! empty($_POST)){
		    if(isset($_POST['nome'])){
		   		 $nome= $_POST['nome'];
		    }	
		}else{
		    $nome="";
		}
	}

	public function get_Nome(){
		return $this->nome;
	}

	public function set_Senha($senha){
		if(! empty($_POST)){
		    if(isset($_POST['senha'])){
		   		 $senha= $_POST['senha'];
		    }	
		}else{
		    $senha="";
		}
	}

	public function get_Senha(){
		return $this->senha;
	}

	public function set_Idgrupo($idgrupo){
		if(! empty($_POST) && is_numeric($idgrupo)){
		    if(isset($_POST['idgrupo'])){
		   		 $idgrupo= $_POST['idgrupo'];
		    }	
		}else{
		    $idgrupo= "";
		}
	}

	public function get_Idgrupo(){
		return $this->idgrupo;
	}

	public function set_Identidade($identidade){
		if(! empty($_POST) && is_numeric($identidade)){
		    if(isset($_POST['identidade'])){
		   		 $identidade= $_POST['identidade'];
		    }	
		}else{
		    $identidade="";
		}
	}

	public function get_Identidade(){
		return $this->identidade;
	}
}
?>