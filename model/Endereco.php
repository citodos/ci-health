<?php

require_once('Entidade.php');
class Endereco{
	
	var $idendereco;
	var $dsendereco;
	var $nrendereco;
	var $bairro;
	var $cidade;
	var $uf;
	var $cep;
	var $complemento;
	var $idlogradouro;
	var $idtipoendereco;

   
	public function enviarEndereco(){
		$entidade = new Entidade;
		$entidade = $_GET['identidade'];

		echo $entidade;
		
	}

	public function set_Idendereco($idendereco){
		if(! empty($idendereco) && is_numeric($idendereco)){
		    if(isset($idendereco)){
		   		 $this->idendereco= $idendereco;
		    }	
		}else{
		    $this->idendereco="";
		}
	}

	public function get_Idendereco(){
		return $this->idendereco;
	}

	public function set_Dsendereco($dsendereco){
		if(! empty($dsendereco)){
		    if(isset($dsendereco)){
		   		 $this->dsendereco= $dsendereco;
		    }	
		}else{
		    $this->dsendereco="";
		}
	}

	public function get_Dsendereco(){
		return $this->dsendereco;
	}

	public function set_Nrendereco($nrendereco){
		if(! empty($nrendereco)){
		    if(isset($nrendereco)){
		   		 $this->nrendereco= $nrendereco;
		    }	
		}else{
		    $this->nrendereco="";
		}
	}

	public function get_Nrendereco(){
		return $this->nrendereco;
	}

	public function set_Bairro($bairro){
		if(! empty($bairro)){
		    if(isset($bairro)){
		   		 $this->bairro= $bairro;
		    }	
		}else{
		    $this->bairro="";
		}
	}

	public function get_Bairro(){
		return $this->bairro;
	}

	public function set_Cidade($cidade){
		if(! empty($cidade)){
		    if(isset($cidade)){
		   		 $this->cidade= $cidade;
		    }	
		}else{
		    $this->cidade="";
		}
	}

	public function get_Cidade(){
		return $this->cidade;
	}

	public function set_Uf($uf){
		if(! empty($uf)){
		    if(isset($uf)){
		   		 $this->uf= $uf;
		    }	
		}else{
		    $this->uf=null;
		}
	}

	public function get_Uf(){
		return $this->uf;
	}

	public function set_Cep($cep){
		if(! empty($cep)){
		    if(isset($cep)){
		   		 $this->cep= $cep;
		    }	
		}else{
		    $this->cep=null;
		}
	}

	public function get_Cep(){
		return $this->cep;
	}

	public function set_Complemento($complemento){
		if(! empty($complemento)){
		    if(isset($complemento)){
		   		 $this->complemento= $complemento;
		    }	
		}else{
		    $this->complemento=null;
		}
	}

	public function get_Complemento(){
		return $this->complemento;
	}

	public function set_Idlogradouro($idlogradouro){
		if(! empty($idlogradouro)){
		    if(isset($idlogradouro)){
		   		 $this->idlogradouro= $idlogradouro;
		    }	
		}else{
		    $this->idlogradouro="";
		}
	}

	public function get_Idlogradouro(){
		return $this->idlogradouro;
	}

	public function set_Idtipoendereco($idtipoendereco){
		if(! empty($idtipoendereco)){
		    if(isset($idtipoendereco)){
		   		 $this->idtipoendereco= $idtipoendereco;
		    }	
		}else{
		    $this->idtipoendereco="";
		}
	}

	public function get_Idtipoendereco(){
		return $this->idtipoendereco;
	}

}

?>






