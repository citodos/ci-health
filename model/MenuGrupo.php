<?php
class MenuGrupo{
	var $idgrupo;
	var $idmenu;

	public function set_Idgrupo($idgrupo){
		if(! empty($_POST) && is_numeric($idgrupo)){
		    if(isset($_POST['idgrupo'])){
		   		 $idgrupo= $_POST['idgrupo'];
		    }	
		}else{
		    $idgrupo= "";
		}
	}

	public function get_Idgrupo(){
		return $this->idgrupo;
	}

	public function set_Idmenu($idmenu){
		if(! empty($_POST) && is_numeric($idmenu)){
		    if(isset($_POST['idmenu'])){
		   		 $idmenu= $_POST['idmenu'];
		    }	
		}else{
		    $idmenu= "";
		}
	}

	public function get_Idmenu(){
		return $this->idmenu;
	}
}
?>