<?php
class AgendaParamProfissional{
	var $idagendaparamprofissional;
	var $identidade;
	var $idespecialidade;
	var $diasemana;
	var $hora_inicio;
	var $hora_fim;
	var $qtde_manha;
	var $periodo_consulta;
	var $hr_ini_tarde;
	var $hr_fim_tarde;
	var $qtd_tarde;
	var $retorno;


	public function set_Idagendaparamprofissional($idagendaparamprofissional){
		if(! empty($_POST) && is_numeric($idagendaparamprofissional)){
		    if(isset($_POST['idagendaparamprofissional'])){
		   		 $idagendaparamprofissional= $_POST['idagendaparamprofissional'];
		    }	
		}else{
		    $idagendaparamprofissional="";
		}
	}

	public function get_Idagendaparamprofissional(){
		return $this->idagendaparamprofissional;
	}

	public function set_Identidade($identidade){
		if(! empty($_POST) && is_numeric($identidade)){
		    if(isset($_POST['identidade'])){
		   		 $identidade= $_POST['identidade'];
		    }	
		}else{
		    $identidade= "";
		}
	}

	public function get_Identidade(){
		return $this->identidade;
	}

	public function set_Idespecialidade($idespecialidade){
		if(! empty($_POST) && is_numeric($idespecialidade)){
		    if(isset($_POST['idespecialidade'])){
		   		 $idespecialidade= $_POST['idespecialidade'];
		    }	
		}else{
		    $idespecialidade= "";
		}
	}

	public function get_Idespecialidade(){
		return $this->idespecialidade;
	}

	public function set_Diasemana($diasemana){
		if(! empty($_POST) && is_numeric($diasemana)){
		    if(isset($_POST['diasemana'])){
		   		 $diasemana= $_POST['diasemana'];
		    }	
		}else{
		    $diasemana= "";
		}
	}

	public function get_Diasemana(){
		return $this->diasemana;
	}

	public function set_Hora_inicio($hora_inicio){
		if(! empty($_POST)){
		    if(isset($_POST['hora_inicio'])){
		   		 $hora_inicio= $_POST['hora_inicio'];
		    }	
		}else{
		    $hora_inicio="";
		}
	}

	public function get_Hora_inicio(){
		return $this->hora_inicio;
	}

	public function set_Hora_fim($hora_fim){
		if(! empty($_POST)){
		    if(isset($_POST['hora_fim'])){
		   		 $hora_fim= $_POST['hora_fim'];
		    }	
		}else{
		    $hora_fim="";
		}
	}

	public function get_Hora_fim(){
		return $this->hora_fim;
	}

	public function set_Qtde_manha($qtde_manha){
		if(! empty($_POST) && is_numeric($qtde_manha)){
		    if(isset($_POST['qtde_manha'])){
		   		 $qtde_manha= $_POST['qtde_manha'];
		    }	
		}else{
		    $qtde_manha ="";
		}
	}

	public function get_qtde_manha(){
		return $this->qtde_manha;
	}

	public function set_Periodo_consulta($periodo_consulta){
		if(! empty($_POST) && is_numeric($periodo_consulta)){
		    if(isset($_POST['periodo_consulta'])){
		   		 $periodo_consulta= $_POST['periodo_consulta'];
		    }	
		}else{
		    $periodo_consulta ="";
		}
	}

	public function get_Periodo_consulta(){
		return $this->periodo_consulta;
	}

	public function set_Hr_ini_tarde($hr_ini_tarde){
		if(! empty($_POST)){
		    if(isset($_POST['hr_ini_tarde'])){
		   		 $hr_ini_tarde= $_POST['hr_ini_tarde'];
		    }	
		}else{
		    $hr_ini_tarde="";
		}
	}

	public function get_Hr_ini_tarde(){
		return $this->hr_ini_tarde;
	}

	public function set_Hr_fim_tarde($hr_fim_tarde){
		if(! empty($_POST)){
		    if(isset($_POST['hr_fim_tarde'])){
		   		 $hr_fim_tarde= $_POST['hr_fim_tarde'];
		    }	
		}else{
		    $hr_fim_tarde="";
		}
	}

	public function get_Hr_fim_tarde(){
		return $this->hr_fim_tarde;
	}

	public function set_Qtd_tarde($qtd_tarde){
		if(! empty($_POST) && is_numeric($qtd_tarde)){
		    if(isset($_POST['qtd_tarde'])){
		   		 $qtd_tarde= $_POST['qtd_tarde'];
		    }	
		}else{
		    $qtd_tarde ="";
		}
	}

	public function get_qtd_tarde(){
		return $this->qtd_tarde;
	}

	public function set_Retorno($retorno){
		if(! empty($_POST) && is_numeric($retorno)){
		    if(isset($_POST['retorno'])){
		   		 $retorno= $_POST['retorno'];
		    }	
		}else{
		    $retorno ="";
		}
	}

	public function get_Retorno(){
		return $this->retorno;
	}
	
}

?>