<?php
class EstadoCivil{
	var $idestcivil;
	var $dsestcivil;

	public function set_Idestcivil($idestcivil){
		if(! empty($_POST) && is_numeric($idestcivil)){
		    if(isset($_POST['idestcivil'])){
		   		 $idestcivil= $_POST['idestcivil'];
		    }	
		}else{
		    $idestcivil= "";
		}
	}

	public function get_Idestcivil(){
		return $this->idestcivil;
	}

	public function set_Dsestcivil($dsestcivil){
		if(! empty($_POST)){
		    if(isset($_POST['dsestcivil'])){
		   		 $dsestcivil= $_POST['dsestcivil'];
		    }	
		}else{
		    $dsestcivil="";
		}
	}

	public function get_Dsestcivil(){
		return $this->dsestcivil;
	}
}
?>