<?php
class Entidade_LocalExecSolic{
	var $idlocal;
	var $identidade;
	var $identidade_operadora;
	var $codoperadora;


	public function set_Idlocal($idlocal){
		if(! empty($idlocal) && is_numeric($idlocal)){
		    if(isset($idlocal)){
		   		 $this->idlocal= $idlocal;
		    }	
		}else{
		    $this->idlocal=0;
		}
	}

	public function get_Idlocal(){
		return $this->idlocal;
	}

	public function set_Identidade($identidade){
		if(! empty($identidade) && is_numeric($identidade)){
		    if(isset($identidade)){
		   		 $this->identidade= $identidade;
		    }	
		}else{
		    $this->identidade=0;
		}
	}

	public function get_Identidade(){
		return $this->identidade;
	}

	public function set_Identidade_operadora($identidade_operadora){
		if(! empty($identidade_operadora && is_numeric($identidade_operadora))){
		    if(isset($identidade_operadora)){
		   		 $this->identidade_operadora= $identidade_operadora;
		    }	
		}else{
		    $this->identidade_operadora=0;
		}
	}

	public function get_Identidade_operadora(){
		return $this->identidade_operadora;
	}

	public function set_Codoperadora($codoperadora){
		if(! empty($codoperadora)){
		    if(isset($codoperadora)){
		   		  $this->codoperadora= $codoperadora;
		    }	
		}else{
		    $this->codoperadora="";
		}
	}

	public function get_Codoperadora(){
		return $this->codoperadora;
	}

}

?>