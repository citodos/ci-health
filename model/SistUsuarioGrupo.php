<?php
class SistUsuarioGrupo{
	var $idgrupo;
	var $idsistema;
	var $idusuario;

	public function set_Idgrupo($idgrupo){
		if(! empty($_POST) && is_numeric($idgrupo)){
		    if(isset($_POST['idgrupo'])){
		   		 $idgrupo= $_POST['idgrupo'];
		    }	
		}else{
		    $idgrupo= "";
		}
	}

	public function get_Idgrupo(){
		return $this->idgrupo;
	}

	public function set_Idsistema($idsistema){
		if(! empty($_POST) && is_numeric($idsistema)){
		    if(isset($_POST['idsistema'])){
		   		 $idsistema= $_POST['idsistema'];
		    }	
		}else{
		    $idsistema= "";
		}
	}

	public function get_Idsistema(){
		return $this->idsistema;
	}

	public function set_Idusuario($idusuario){
		if(! empty($_POST) && is_numeric($idusuario)){
		    if(isset($_POST['idusuario'])){
		   		 $idusuario= $_POST['idusuario'];
		    }	
		}else{
		    $idusuario= "";
		}
	}

	public function get_Idusuario(){
		return $this->idusuario;
	}
}
?>