<?php
class AgendaMov{
	var $idagendamov;
	var $identidade_paciente;
	var $identidade_profissional;
	var $identidade_unidade;
	var $idespecialidade;
	var $dtconsulta;
	var $horaconsulta;
	var $minutoconsulta;
	var $diasemana;
	var $flg_retorno;
	var $flg_reserva;
	var $dtcadastro;
	var $dtcancelamento;
	var $flg_presenca;


	public function set_Idagendamov($idagendamov){
		if(! empty($_POST) && is_numeric($idagendamov)){
		    if(isset($_POST['idagendamov'])){
		   		 $idagendamov= $_POST['idagendamov'];
		    }	
		}else{
		    $idagendamov="";
		}
	}

	public function get_Idagendamov(){
		return $this->idagendamov;
	}

	public function set_Identidade_paciente($identidade_paciente){
		if(! empty($_POST) && is_numeric($identidade_paciente)){
		    if(isset($_POST['identidade_paciente'])){
		   		 $identidade_paciente= $_POST['identidade_paciente'];
		    }	
		}else{
		    $identidade_paciente="";
		}
	}

	public function get_Identidade_paciente(){
		return $this->identidade_paciente;
	}

	public function set_Identidade_profissional($identidade_profissional){
		if(! empty($_POST) && is_numeric($identidade_profissional)){
		    if(isset($_POST['identidade_profissional'])){
		   		 $identidade_profissional= $_POST['identidade_profissional'];
		    }	
		}else{
		    $identidade_profissional="";
		}
	}

	public function get_Identidade_profissional(){
		return $this->identidade_profissional;
	}

	public function set_Identidade_unidade($identidade_unidade){
		if(! empty($_POST) && is_numeric($identidade_unidade)){
		    if(isset($_POST['identidade_unidade'])){
		   		 $identidade_unidade= $_POST['identidade_unidade'];
		    }	
		}else{
		    $identidade_unidade="";
		}
	}

	public function get_Identidade_unidade(){
		return $this->identidade_unidade;
	}

	public function set_Idespecialidade($idespecialidade){
		if(! empty($_POST) && is_numeric($idespecialidade)){
		    if(isset($_POST['idespecialidade'])){
		   		 $idespecialidade= $_POST['idespecialidade'];
		    }	
		}else{
		    $idespecialidade="";
		}
	}

	public function get_Idespecialidade(){
		return $this->idespecialidade;
	}

	public function set_Dtconsulta($dtconsulta){
		if(! empty($_POST)){
		    if(isset($_POST['dtconsulta'])){
		   		 $dtconsulta= $_POST['dtconsulta'];
		    }	
		}else{
		    $dtconsulta="";
		}
	}

	public function get_Dtconsulta(){
		return $this->dtconsulta;
	}

	public function set_Horaconsulta($horaconsulta){
		if(! empty($_POST)){
		    if(isset($_POST['horaconsulta'])){
		   		 $horaconsulta= $_POST['horaconsulta'];
		    }	
		}else{
		    $horaconsulta="";
		}
	}

	public function get_Horaconsulta(){
		return $this->horaconsulta;
	}

	public function set_Minutoconsulta($minutoconsulta){
		if(! empty($_POST)){
		    if(isset($_POST['minutoconsulta'])){
		   		 $minutoconsulta= $_POST['minutoconsulta'];
		    }	
		}else{
		    $minutoconsulta="";
		}
	}

	public function get_Minutoconsulta(){
		return $this->minutoconsulta;
	}

	public function set_Diasemana($diasemana){
		if(! empty($_POST) && is_numeric($diasemana)){
		    if(isset($_POST['diasemana'])){
		   		 $diasemana= $_POST['diasemana'];
		    }	
		}else{
		    $diasemana= "";
		}
	}

	public function get_Diasemana(){
		return $this->diasemana;
	}

	public function set_Flg_retorno($flg_retorno){
		if(! empty($_POST)){
		    if(isset($_POST['flg_retorno'])){
		   		 $flg_retorno= $_POST['flg_retorno'];
		    }	
		}else{
		    $flg_retorno="";
		}
	}

	public function get_Flg_retorno(){
		return $this->flg_retorno;
	}

	public function set_Flg_reserva($flg_reserva){
		if(! empty($_POST)){
		    if(isset($_POST['flg_reserva'])){
		   		 $flg_reserva= $_POST['flg_reserva'];
		    }	
		}else{
		    $flg_reserva="";
		}
	}

	public function get_Flg_reserva(){
		return $this->flg_reserva;
	}

	public function set_Dtcadastro($dtcadastro){
		if(! empty($_POST)){
		    if(isset($_POST['dtcadastro'])){
		   		 $dtcadastro= $_POST['dtcadastro'];
		    }	
		}else{
		    $dtcadastro="";
		}
	}

	public function get_Dtcadastro(){
		return $this->dtcadastro;
	}

	public function set_Dtcancelamento($dtcancelamento){
		if(! empty($_POST)){
		    if(isset($_POST['dtcancelamento'])){
		   		 $dtcancelamento= $_POST['dtcancelamento'];
		    }	
		}else{
		    $dtcancelamento="";
		}
	}

	public function get_Dtcancelamento(){
		return $this->dtcancelamento;
	}

	public function set_Flg_presenca($flg_presenca){
		if(! empty($_POST)){
		    if(isset($_POST['flg_presenca'])){
		   		 $flg_presenca= $_POST['flg_presenca'];
		    }	
		}else{
		    $flg_presenca="";
		}
	}

	public function get_Flg_presenca(){
		return $this->flg_presenca;
	}

}

?>