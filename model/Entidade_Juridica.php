<?php

class Entidade_Juridica{

	var $nomefantasia;
	var $cnpj;
	var $inscr_estadual;
	var $inscr_municipal;
	var $flg_hub;
	var $cnes;
	var $logo;
	var $dtultvalidacaonet;
	var $codigoibge;
	var $contrato;
	var $limitediasagenda;
	var $timerparaagendamento;
	var $identidade_matriz;
	

	public function set_Nomefantasia($nomefantasia){
    	if(! empty($nomefantasia)){
    if(isset($nomefantasia)){
  	  $this->nomefantasia= $nomefantasia;
    }
	}else{
	    $this->nomefantasia=null;
	}
    }	

	public function get_Nomefantasia(){
		return $this->nomefantasia;
	}

	public function set_Cnpj($cnpj){
		if(!empty($cnpj)){
    if(isset($cnpj)){
   		$this->cnpj= $cnpj;
    }
	}else{
	    $this->cnpj="";
	}
	}

	public function get_Cnpj(){
		return $this->cnpj;
	}

	public function set_Inscr_estadual($inscr_estadual){
		if(! empty($inscr_estadual)){
    if(isset($inscr_estadual)){
  	  $this->inscr_estadual= $inscr_estadual;
    }
	}else{
	    $this->inscr_estadual=null;
	}
	}

	public function get_Inscr_estadual(){
		return $this->inscr_estadual;
	}

	public function set_Inscr_municipal($inscr_municipal){
		if(! empty($inscr_municipal)){
    if(isset($inscr_municipal)){
   		 $this->inscr_municipal= $inscr_municipal;
    }
	}else{
	    $this->inscr_municipal=null;
	}
	}

	public function get_Inscr_municipal(){
		return $this->inscr_municipal;
	}

	public function set_Flg_hub($flg_hub){
		if(! empty($flg_hub)){
		    if(isset($flg_hub)){
		   		 $this->flg_hub= $flg_hub;
		    }
		}else{
	   		 $this->flg_hub="";
		}
	}

	public function get_Flg_hub(){
		return $this->flg_hub;
	}

	public function set_Cnes($cnes){
		if(! empty($cnes)){
		    if(isset($cnes)){
		   		 $this->cnes= $cnes;
		    }
		}else{
	   		 $this->cnes="";
		}
	}

	public function get_Cnes(){
		return $this->cnes;
	}

	public function set_Logo($logo){
		if(! empty($logo)){
		    if(isset($logo)){
		   		 $this->logo= $logo;
		    }
		}else{
	   		 $this->logo="";
		}
	}

	public function get_Logo(){
		return $this->logo;
	}

	public function set_Dtultvalidacaonet($dtultvalidacaonet){
		if(! empty($dtultvalidacaonet)){
		    if(isset($dtultvalidacaonet)){
		   		 $this->dtultvalidacaonet= $dtultvalidacaonet;
		    }
		}else{
	   		 $this->dtultvalidacaonet="";
		}
	}

	public function get_Dtultvalidacaonet(){
		return $this->dtultvalidacaonet;
	}

	public function set_Codigoibge($codigoibge){
		if(! empty($codigoibge)){
		    if(isset($codigoibge)){
		   		 $this->codigoibge= $codigoibge;
		    }
		}else{
	   		 $this->codigoibge="";
		}
	}

	public function get_Codigoibge(){
		return $this->codigoibge;
	}

	public function set_Contrato($contrato){
		if(! empty($contrato) && is_numeric($identidade)){
		    if(isset($contrato)){
		   		 $this->contrato= $contrato;
		    }
		}else{
	   		 $this->contrato="";
		}
	}

	public function get_Contrato(){
		return $this->contrato;
	}

	public function set_Limitediasagenda($limitediasagenda){
		if(! empty($limitediasagenda) && is_numeric($limitediasagenda)){
		    if(isset($limitediasagenda)){
		   		 $this->limitediasagenda= $limitediasagenda;
		    }
		}else{
	   		 $this->limitediasagenda="";
		}
	}

	public function get_Limitediasagenda(){
		return $this->limitediasagenda;
	}

	public function set_Timerparaagendamento($timerparaagendamento){
		if(! empty($timerparaagendamento) && is_numeric($timerparaagendamento)){
		    if(isset($timerparaagendamento)){
		   		 $this->timerparaagendamento= $timerparaagendamento;
		    }
		}else{
	   		 $this->timerparaagendamento="";
		}
	}

	public function get_Timerparaagendamento(){
		return $this->timerparaagendamento;
	}

	public function set_Identidade_matriz($identidade_matriz){
		if(! empty($identidade_matriz) && is_numeric($identidade_matriz)){
		    if(isset($identidade_matriz)){
		   		 $this->identidade_matriz= $identidade_matriz;
		    }
		}else{
	   		 $this->identidade_matriz="";
		}
	}

	public function get_Identidade_matriz(){
		return $this->identidade_matriz;
	}

}


?>