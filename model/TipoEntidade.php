<?php
class TipoEntidade{
	var $idtipo;
	var $dstipo;

	public function set_Idtipo($idtipo){
		if(! empty($_POST) && is_numeric($idtipo)){
		    if(isset($_POST['idtipo'])){
		   		 $idtipo= $_POST['idtipo'];
		    }	
		}else{
		    $idtipo= "";
		}
	}

	public function get_Idtipo(){
		return $this->idtipo;
	}

	public function set_Dstipo($dstipo){
		if(! empty($_POST)){
		    if(isset($_POST['dstipo'])){
		   		 $dstipo= $_POST['dstipo'];
		    }	
		}else{
		    $dstipo="";
		}
	}

	public function get_Dstipo(){
		return $this->dstipo;
	}
}
?>