//funções de javascript para validar os campos de cadastro da Empresa 

function valida() {
	

if (document.frmempresa.dsorg.value.length <= 5 ) {
alert("Preencha o Nome da Empresa.");
document.frmempresa.dsorg.focus();
return false;
}

if (document.frmempresa.nome_fantasia.value.length <= 5) {
alert("Preencha o Nome Fantasia da Empresa.");
document.frmempresa.nome_fantasia.focus();
return false;
}
if (document.frmempresa.cnpj.value=="") {

alert("Preencha o CNPJ da Empresa.");
document.frmempresa.cnpj.focus();
	
	return false;

}

}
function validaEndereco(){
    if (document.frmendereco.tipoEndereco.value== "") {
    alert("Informe o tipo de endereço da Empresa.");
    document.frmendereco.tipoEndereco.focus();
    return false;
    }
    if (document.frmendereco.tipoLogradouro.value== "") {
    alert("Informe o tipo de logradouro da Empresa.");
    document.frmendereco.tipoLogradouro.focus();
    return false;
    }

    if (document.frmendereco.dsendereco.value.length < 3 ) {
    alert("Preencha o Endereço da Empresa.");
    document.frmendereco.dsendereco.focus();
    return false;
    }
    if (document.frmendereco.nrendereco.value== "") {
    alert("Preencha o número do endereço da Empresa.");
    document.frmendereco.nrendereco.focus();
    return false;
    }
    if (document.frmendereco.cep.value=="") {
    alert("Preencha o CEP da Empresa.");
    document.frmendereco.cep.focus();
    return false;
   
    }

     if (document.frmendereco.bairro.value== "") {
    alert("Preencha o Bairro da Empresa.");
    document.frmendereco.bairro.focus();
    return false;
    }
    if (document.frmendereco.cidade.value== "") {
    alert("Preencha o Municipio da Empresa.");
    document.frmendereco.cidade.focus();
    return false;
    }
    if (document.frmendereco.uf.value== "") {
    alert("Preencha o Estado da Empresa.");
    document.frmendereco.uf.focus();
    return false;
    }
}

//adiciona mascara de cep
function MascaraCep(cep){
                if(mascaraInteiro(cep)==false){
                event.returnValue = false;
        }       
        return formataCampo(cep, '00000-000', event);
}

//adiciona mascara de data
function MascaraData(data){
        if(mascaraInteiro(data)==false){
                event.returnValue = false;
        }       
        return formataCampo(data, '00/00/0000', event);
}

//adiciona mascara ao telefone
function MascaraTelefone(tel){  
        if(mascaraInteiro(tel)==false){
                event.returnValue = false;
        }       
        return formataCampo(tel, '(00) 0000-0000', event);
}
//adiciona mascara de cnpj
function MascaraCNPJ(cnpj){
        if(mascaraInteiro(cnpj)==false){
                event.returnValue = false;
        }       
        return formataCampo(cnpj, '00.000.000/0000-00', event);
}



//valida numero inteiro com mascara
function mascaraInteiro(){
        if (event.keyCode < 48 || event.keyCode > 57){
                event.returnValue = false;
                return false;
        }
        return true;
}



//valida o CNPJ digitado
function ValidarCNPJ(ObjCnpj){
        var cnpj = ObjCnpj.value;
        var valida = new Array(6,5,4,3,2,9,8,7,6,5,4,3,2);
        var dig1= new Number;
        var dig2= new Number;

        exp = /\.|\-|\//g;
        cnpj = cnpj.toString().replace( exp, "" ); 
        var digito = new Number(eval(cnpj.charAt(12)+cnpj.charAt(13)));

        for(i = 0; i<valida.length; i++){
                dig1 += (i>0? (cnpj.charAt(i-1)*valida[i]):0);  
                dig2 += cnpj.charAt(i)*valida[i];       
        }
        dig1 = (((dig1%11)<2)? 0:(11-(dig1%11)));
        dig2 = (((dig2%11)<2)? 0:(11-(dig2%11)));

        if(((dig1*10)+dig2) != digito || cnpj.toString() == "00000000000000")  
                alert('CNPJ Invalido!');

}


//valida telefone
function ValidaTelefone(tel){
        exp = /\(\d{2}\)\ \d{4}\-\d{4}/
        if(!exp.test(tel.value))
                alert('Numero de Telefone Invalido!');
}



//valida CEP
function ValidaCep(cep){
        exp = /\d{5}\-\d{3}/
        if(!exp.test(cep.value)){
                alert('Numero de Cep Invalido!');               
            }
}

//valida data
function ValidaData(data){
        exp = /\d{2}\/\d{2}\/\d{4}/
        if(!exp.test(data.value))
                alert('Data Invalida!');                        
}


//formata de forma generica os campos
function formataCampo(campo, Mascara, evento) { 
        var boleanoMascara; 

        var Digitato = evento.keyCode;
        exp = /\-|\.|\/|\(|\)| /g
        campoSoNumeros = campo.value.toString().replace( exp, "" ); 

        var posicaoCampo = 0;    
        var NovoValorCampo="";
        var TamanhoMascara = campoSoNumeros.length;; 

        if (Digitato != 8) { // backspace 
                for(i=0; i<= TamanhoMascara; i++) { 
                        boleanoMascara  = ((Mascara.charAt(i) == "-") || (Mascara.charAt(i) == ".")
                                                                || (Mascara.charAt(i) == "/")) 
                        boleanoMascara  = boleanoMascara || ((Mascara.charAt(i) == "(") 
                                                                || (Mascara.charAt(i) == ")") || (Mascara.charAt(i) == " ")) 
                        if (boleanoMascara) { 
                                NovoValorCampo += Mascara.charAt(i); 
                                  TamanhoMascara++;
                        }else { 
                                NovoValorCampo += campoSoNumeros.charAt(posicaoCampo); 
                                posicaoCampo++; 
                          }              
                  }      
                campo.value = NovoValorCampo;
                  return true; 
        }else { 
                return true; 
        }
}
