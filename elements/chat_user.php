<div class=chat-user-list>
          <form class="form-horizontal chat-search" role=form>
            <div class=form-group>
              <input class=form-control placeholder="Search for user...">
              <button type=submit><i class="ec-search s16"></i></button>
            </div>
            <!-- End .form-group  -->



          </form>

            
          <ul class="chat-ui bsAccordion">
            <li><a href=#>Favorites <span class="notification teal">4</span><i class=en-arrow-down5></i></a>
              <ul class=in>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/49.jpg alt=@chadengle>Chad Engle <span class=has-message><i class=im-pencil></i></span></a> <span class="status online"><i class=en-dot></i></span></li>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/54.jpg alt=@alagoon>Anthony Lagoon</a> <span class="status offline"><i class=en-dot></i></span></li>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/52.jpg alt=@koridhandy>Kory Handy</a> <span class=status><i class=en-dot></i></span></li>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/50.jpg alt=@divya>Divia Manyan</a> <span class=status><i class=en-dot></i></span></li>
              </ul>
            </li>
            <li><a href=#>Online <span class="notification green">3</span><i class=en-arrow-down5></i></a>
              <ul class=in>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/51.jpg alt=@kolage>Eric Hofman</a> <span class="status online"><i class=en-dot></i></span></li>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/55.jpg alt=@mikebeecham>Mike Beecham</a> <span class="status online"><i class=en-dot></i></span></li>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/53.jpg alt=@derekebradley>Darek Bradly</a> <span class="status online"><i class=en-dot></i></span></li>
              </ul>
            </li>
            <li><a href=#>Offline <span class="notification red">5</span><i class=en-arrow-down5></i></a>
              <ul>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/56.jpg alt=@laurengray>Lauren Grey</a> <span class="status offline"><i class=en-dot></i></span></li>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/49.jpg alt=@chadengle>Chad Engle</a> <span class="status offline"><i class=en-dot></i></span></li>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/58.jpg alt=@frankiefreesbie>Frankie Freesibie</a> <span class="status offline"><i class=en-dot></i></span></li>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/57.jpg alt=@joannefournier>Joane Fornier</a> <span class="status offline"><i class=en-dot></i></span></li>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/59.jpg alt=@aiiaiiaii>Alia Alien</a> <span class="status offline"><i class=en-dot></i></span></li>
              </ul>
            </li>
          </ul>
        </div>