
<!-- Start #sidebar -->
<div id='sidebar'>
  <!-- Start .sidebar-inner -->
  <div class='sidebar-inner'>
    <!-- Start #sideNav -->
    <ul id='sideNav' class="nav nav-pills nav-stacked">
      <li class='top-search'>
        <form>
          <input name='search' placeholder="Pesquisa ...">
          <button type='submit'><i class="ec-search s20"></i></button>
        </form>
      </li>
      <li><a href='cadpaciente.php'>Cadastro de Paciente <i class=''></i></a></li>
          <li><a href='cadmedico.php'>Cadastro de Profissionais <i class=''></i></a></li>
                <li><a href='cadempresa.php'>Cadastro de Empresa<i class=''></i></a></li>
      <li><a href='index.php'>Dashboard <i class='im-screen'></i></a></li>
 <li><a href='cad-unidade.php'>Cadastro de Unidades <i class=></i></a></li>
      <li><a href='charts.php'>Charts <i class='st-chart'></i></a></li>
      <li><a href='#'>Forms <i class='im-paragraph-justify'></i></a>
        <ul class="nav sub">
          <li><a href='forms.php'><i class='ec-pencil2'></i> Form Stuff</a></li>
          <li><a href='form-validation.php'><i class='im-checkbox-checked'></i> Form Validation</a></li>
          <li><a href='form-wizard.php'><i class='im-wand'></i> Form Wizard</a></li>
          <li><a href='wysiwyg.php'><i class='fa-pencil'></i> WYSIWYG editor</a></li>
        </ul>
      </li>
      <li><a href='#'>Tables <i class='im-table2'></i></a>
        <ul class="nav sub">
          <li><a href='tables.php'><i class='en-arrow-right7'></i> Static tables</a></li>
          <li><a href='data-tables.php'><i class='en-arrow-right7'></i> Data tables</a></li>
        </ul>
      </li>
      <li><a href='#'>UI Elements <i class='st-lab'></i></a>
        <ul class="nav sub">
          <li><a href='notifications.php'><i class='fa-bell'></i> Notifications</a></li>
          <li><a href='panels.php'><i class='br-window'></i> Panels</a></li>
          <li><a href='tiles.php'><i class='im-windows8'></i> Tiles</a></li>
          <li><a href='elements.php'><i class='st-cube'></i> Elements</a></li>
          <li><a href='icons.php'><i class='im-stack'></i> Icons</a></li>
          <li><a href='buttons.php'><i class='im-play2'></i> Buttons</a></li>
          <li><a href='calendar.php'><i class='im-calendar2'></i> Calendar</a></li>
          <li><a href='grid.php'><i class='st-grid'></i> Grid</a></li>
          <li><a href='typo.php'><i class='im-font'></i> Typography</a></li>
          <li><a href='list.php'><i class='fa-list'></i> Lists</a></li>
        </ul>
      </li>
      <li><a href='#'><i class='ec-mail'></i> Email app</a>
        <ul class="nav sub">
          <li><a href='email-inbox.php'><i class='ec-archive'></i> Inbox</a></li>
          <li><a href='email-read.php'><i class='br-eye'></i> Read email</a></li>
          <li><a href='email-write.php'><i class='ec-pencil2'></i> Write email</a></li>
        </ul>
      </li>
      <li><a href='file.php'><i class='en-upload'></i> File Manager</a></li>
      <li><a href='gallery.php'><i class='im-images'></i> Gallery</a></li>
      <li><a href='widgets.php'><i class='st-diamond'></i> Widgets</a></li>
      <li><a href='#'><i class='ec-location'></i> Maps</a>
        <ul class="nav sub">
          <li><a href='maps-google.php'><i class='im-map2'></i> Google maps</a></li>
          <li><a href='maps-vector.php'><i class='en-location2'></i> Vector maps</a></li>
        </ul>
      </li>
      <li><a href='#'>Pages <i class='st-files'></i></a>
        <ul class="nav sub">
          <li><a href='timeline.php'><i class='ec-clock'></i> Timeline page</a></li>
          <li><a href='invoice.php'><i class='st-file'></i> Invoice</a></li>
          <li><a href='profile.php'><i class='ec-user'></i> Profile page</a></li>
          <li><a href='search.php'><i class='ec-search'></i> Search page</a></li>
          <li><a href='blank.php'><i class='im-file4'></i> Blank page</a></li>
          <li><a href='login.php'><i class='ec-locked'></i> Login page</a></li>
          <li><a href='lockscreen.php'><i class='ec-locked'></i> Lock screen</a></li>
          <li><a href='#'><i class='st-files'></i> Error pages</a>
            <ul class="nav sub">
              <li><a href='400.php'><i class='st-file-broken'></i> Error 400</a></li>
              <li><a href='401.php'><i class='st-file-broken'></i> Error 401</a></li>
              <li><a href='403.php'><i class='st-file-broken'></i> Error 403</a></li>
              <li><a href='404.php'><i class='st-file-broken'></i> Error 404</a></li>
              <li><a href='405.php'><i class='st-file-broken'></i> Error 405</a></li>
              <li><a href='500.php'><i class='st-file-broken'></i> Error 500</a></li>
              <li><a href='503.php'><i class='st-file-broken'></i> Error 503</a></li>
              <li><a href='offline.php'><i class='st-window'></i> Offline</a></li>
            </ul>
          </li>
        </ul>
      </li>
    </ul>
    <!-- End #sideNav -->
    <!-- Start .sidebar-panel -->
    <div class='sidebar-panel'>
      <h4 class='sidebar-panel-title'><i class='im-fire'></i> Server usage</h4>
      <div class='sidebar-panel-content'>
        <ul class='server-stats'>
          <li><span class='txt'>Disk space</span> <span class='percent'>78</span>
            <div id='usage-sparkline' class='sparkline'>Loading...</div>
            <div class='pie-chart' data-percent=78></div>
          </li>
        </ul>
        <ul class='server-stats'>
          <li><span class='txt'>CPU</span> <span class='percent'>56</span>
            <div id='cpu-sparkline' class='sparkline'>Loading...</div>
            <div class='pie-chart' data-percent=56></div>
          </li>
        </ul>
        <ul class='server-stats'>
          <li><span class='txt'>Memory</span> <span class='percent'>14</span>
            <div id='ram-sparkline' class='sparkline'>Loading...</div>
            <div class='pie-chart' data-percent=14></div>
          </li>
        </ul>
      </div>
    </div>
    <!-- End .sidebar-panel -->
  </div>
  <!-- End .sidebar-inner -->
</div>
<!-- End #sidebar -->
