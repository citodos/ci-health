<!DOCTYPE html>
<html lang=en>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
<meta charset=utf-8>
<title>Read email | sprFlat - Admin Template</title>
<!-- Mobile specific metas -->
<meta name=viewport content="width=device-width,initial-scale=1,maximum-scale=1">
<!-- Force IE9 to render in normal mode -->
<!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
<meta name=author content=SuggeElson>
<meta name=description content="sprFlat admin template - new premium responsive admin template. This template is designed to help you build the site administration without losing valuable time.Template contains all the important functions which must have one backend system.Build on great twitter boostrap framework">
<meta name=keywords content="admin, admin template, admin theme, responsive, responsive admin, responsive admin template, responsive theme, themeforest, 960 grid system, grid, grid theme, liquid, jquery, administration, administration template, administration theme, mobile, touch , responsive layout, boostrap, twitter boostrap">
<meta name=application-name content="sprFlat admin template">
<!-- Import google fonts - Heading first/ text second -->
<link rel=stylesheet type=text/css href="http://fonts.googleapis.com/css?family=Open+Sans:400,700|Droid+Sans:400,700">
<!--[if lt IE 9]>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400" rel="stylesheet" type="text/css" />
<link href="http://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet" type="text/css" />
<link href="http://fonts.googleapis.com/css?family=Droid+Sans:400" rel="stylesheet" type="text/css" />
<link href="http://fonts.googleapis.com/css?family=Droid+Sans:700" rel="stylesheet" type="text/css" />
<![endif]-->
<!-- Css files -->
<link rel=stylesheet href=assets/css/main.min.css>
<!-- Fav and touch icons -->
<link rel=apple-touch-icon-precomposed sizes=144x144 href=assets/img/ico/apple-touch-icon-144-precomposed.png>
<link rel=apple-touch-icon-precomposed sizes=114x114 href=assets/img/ico/apple-touch-icon-114-precomposed.png>
<link rel=apple-touch-icon-precomposed sizes=72x72 href=assets/img/ico/apple-touch-icon-72-precomposed.png>
<link rel=apple-touch-icon-precomposed href=assets/img/ico/apple-touch-icon-57-precomposed.png>
<link rel=icon href=assets/img/ico/favicon.ico type=image/png>
<!-- Windows8 touch icon ( http://www.buildmypinnedsite.com/ )-->
<meta name=msapplication-TileColor content=#3399cc>
<body>

<?php include ("elements/header.php"); ?>

<?php include ("elements/sidebar.php"); ?>
<!-- Start #right-sidebar -->
<div id=right-sidebar class=hide-sidebar>
  <!-- Start .sidebar-inner -->
  <div class=sidebar-inner>
    <div class="sidebar-panel mt0">
      <div class="sidebar-panel-content fullwidth pt0">
        <div class=chat-user-list>
          <form class="form-horizontal chat-search" role=form>
            <div class=form-group>
              <input class=form-control placeholder="Search for user...">
              <button type=submit><i class="ec-search s16"></i></button>
            </div>
            <!-- End .form-group  -->
          </form>
          <ul class="chat-ui bsAccordion">
            <li><a href=#>Favorites <span class="notification teal">4</span><i class=en-arrow-down5></i></a>
              <ul class=in>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/49.jpg alt=@chadengle>Chad Engle <span class=has-message><i class=im-pencil></i></span></a> <span class="status online"><i class=en-dot></i></span></li>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/54.jpg alt=@alagoon>Anthony Lagoon</a> <span class="status offline"><i class=en-dot></i></span></li>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/52.jpg alt=@koridhandy>Kory Handy</a> <span class=status><i class=en-dot></i></span></li>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/50.jpg alt=@divya>Divia Manyan</a> <span class=status><i class=en-dot></i></span></li>
              </ul>
            </li>
            <li><a href=#>Online <span class="notification green">3</span><i class=en-arrow-down5></i></a>
              <ul class=in>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/51.jpg alt=@kolage>Eric Hofman</a> <span class="status online"><i class=en-dot></i></span></li>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/55.jpg alt=@mikebeecham>Mike Beecham</a> <span class="status online"><i class=en-dot></i></span></li>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/53.jpg alt=@derekebradley>Darek Bradly</a> <span class="status online"><i class=en-dot></i></span></li>
              </ul>
            </li>
            <li><a href=#>Offline <span class="notification red">5</span><i class=en-arrow-down5></i></a>
              <ul>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/56.jpg alt=@laurengray>Lauren Grey</a> <span class="status offline"><i class=en-dot></i></span></li>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/49.jpg alt=@chadengle>Chad Engle</a> <span class="status offline"><i class=en-dot></i></span></li>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/58.jpg alt=@frankiefreesbie>Frankie Freesibie</a> <span class="status offline"><i class=en-dot></i></span></li>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/57.jpg alt=@joannefournier>Joane Fornier</a> <span class="status offline"><i class=en-dot></i></span></li>
                <li><a href=# class=chat-name><img class=chat-avatar src=assets/img/avatars/59.jpg alt=@aiiaiiaii>Alia Alien</a> <span class="status offline"><i class=en-dot></i></span></li>
              </ul>
            </li>
          </ul>
        </div>
        <div class=chat-box>
          <h5>Chad Engle</h5>
          <a id=close-user-chat href=# class="btn btn-xs btn-primary"><i class=en-arrow-left4></i></a>
          <ul class="chat-ui chat-messages">
            <li class=chat-user>
              <p class=avatar><img src=assets/img/avatars/49.jpg alt=@chadengle></p>
              <p class=chat-name>Chad Engle <span class=chat-time>15 seconds ago</span></p>
              <span class="status online"><i class=en-dot></i></span>
              <p class=chat-txt>Hello Sugge check out the last order.</p>
            </li>
            <li class=chat-me>
              <p class=avatar><img src=assets/img/avatars/48.jpg alt=SuggeElson></p>
              <p class=chat-name>SuggeElson <span class=chat-time>10 seconds ago</span></p>
              <span class="status online"><i class=en-dot></i></span>
              <p class=chat-txt>Ok i will check it out.</p>
            </li>
            <li class=chat-user>
              <p class=avatar><img src=assets/img/avatars/49.jpg alt=@chadengle></p>
              <p class=chat-name>Chad Engle <span class=chat-time>now</span></p>
              <span class="status online"><i class=en-dot></i></span>
              <p class=chat-txt>Thank you, have a nice day</p>
            </li>
          </ul>
          <div class=chat-write>
            <form action=# class=form-horizontal role=form>
              <div class=form-group>
                <textarea name=sendmsg id=sendMsg class="form-control elastic" rows=1></textarea>
                <a role=button class=btn id=attach_photo_btn><i class="fa-picture s20"></i></a>
                <input type=file name=attach_photo id=attach_photo>
              </div>
              <!-- End .form-group  -->
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End .sidebar-inner -->
</div>
<!-- End #right-sidebar -->
<!-- Start #content -->
<div id=content>
  <!-- Start .content-wrapper -->
  <div class=content-wrapper>
    <div class=row>
      <!-- Start .row -->
      <!-- Start .page-header -->
      <div class="col-lg-12 heading">
        <h1 class=page-header><i class=ec-archive></i> Inbox</h1>
        <!-- Start .bredcrumb -->
        <ul id=crumb class=breadcrumb>
        </ul>
        <!-- End .breadcrumb -->
        <!-- Start .option-buttons -->
        <div class=option-buttons>
          <div class=btn-toolbar role=toolbar>
            <div class="btn-group dropdown"><a class="btn dropdown-toggle" data-toggle=dropdown id=dropdownMenu1><i class="br-grid s24"></i></a>
              <div class="dropdown-menu pull-right" role=menu aria-labelledby=dropdownMenu1>
                <div class=option-dropdown>
                  <div class=shortcut-button><a href=#><i class=im-pie></i> <span>Earning Stats</span></a></div>
                  <div class=shortcut-button><a href=#><i class="ec-images color-dark"></i> <span>Gallery</span></a></div>
                  <div class=shortcut-button><a href=#><i class="en-light-bulb color-orange"></i> <span>Fresh ideas</span></a></div>
                  <div class=shortcut-button><a href=#><i class="ec-link color-blue"></i> <span>Links</span></a></div>
                  <div class=shortcut-button><a href=#><i class="ec-support color-red"></i> <span>Support</span></a></div>
                  <div class=shortcut-button><a href=#><i class="st-lock color-teal"></i> <span>Lock area</span></a></div>
                </div>
              </div>
            </div>
            <div class="btn-group dropdown"><a class="btn dropdown-toggle" data-toggle=dropdown id=dropdownMenu2><i class="ec-pencil s24"></i></a>
              <div class="dropdown-menu pull-right" role=menu aria-labelledby=dropdownMenu2>
                <div class=option-dropdown>
                  <div class=row>
                    <p class=col-lg-12>Quick post</p>
                    <form class=form-horizontal role=form>
                      <div class=form-group>
                        <div class=col-lg-12>
                          <input class=form-control placeholder="Enter title">
                        </div>
                      </div>
                      <!-- End .form-group  -->
                      <div class=form-group>
                        <div class=col-lg-12>
                          <textarea class="form-control wysiwyg" placeholder="Enter text"></textarea>
                        </div>
                      </div>
                      <!-- End .form-group  -->
                      <div class=form-group>
                        <div class=col-lg-12>
                          <input class="form-control tags1" placeholder="Enter tags">
                        </div>
                      </div>
                      <!-- End .form-group  -->
                      <div class=form-group>
                        <div class=col-lg-12>
                          <button class="btn btn-default btn-xs">Save Draft</button>
                          <button class="btn btn-success btn-xs pull-right">Publish</button>
                        </div>
                      </div>
                      <!-- End .form-group  -->
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <div class=btn-group><a class="btn dropdown-toggle" data-toggle=dropdown id=dropdownMenu3><i class="ec-help s24"></i></a>
              <div class="dropdown-menu pull-right" role=menu aria-labelledby=dropdownMenu3>
                <div class=option-dropdown>
                  <p>First time visitor ? <a href=# id=app-tour class="btn btn-success ml15">Take app tour</a></p>
                  <hr>
                  <p>Or check the <a href=# class="btn btn-danger ml15">FAQ</a></p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- End .option-buttons -->
      </div>
      <!-- End .page-header -->
    </div>
    <!-- End .row -->
    <div class=outlet>
      <!-- Start .outlet -->
      <!-- Page start here -->
      <div id=email-sidebar>
        <!-- Start #email-sidebar -->
        <div class=p15><a id=write-email href=email-write.html class="btn btn-danger btn-block uppercase"><i class=im-quill></i> compose</a></div>
        <ul id=email-nav class="nav nav-pills nav-stacked">
          <li><a href=email-inbox.html><i class=ec-archive></i> Inbox <span class="notification blue">27</span></a></li>
          <li><a href=#><i class=ec-star></i> Starred <span class="notification orange">2</span></a></li>
          <li><a href=#><i class=ec-info></i> Important <span class=notification>5</span></a></li>
          <li><a href=#><i class=en-location2></i> Send <span class="notification green">14</span></a></li>
          <li><a href=#><i class=ec-pencil2></i> Drafts <span class="notification brown">1</span></a></li>
          <li><a href=#><i class=ec-trashcan></i> Trash <span class="notification dark">3</span></a></li>
          <li class=nav-header>Labels</li>
          <li><a href=#><span class=circle></span> Work</a></li>
          <li><a href=#><span class="circle color-red"></span> Private</a></li>
          <li><a href=#><span class="circle color-green"></span> Travel</a></li>
          <li><a href=#><span class="circle color-pink"></span> Promotions</a></li>
          <li><a href=#><span class="circle color-teal"></span> Updates</a></li>
        </ul>
      </div>
      <!--End #email-sidebar -->
      <div id=email-content>
        <!-- Start #email-content -->
        <div class=email-wrapper>
          <!-- Start .email-wrapper -->
          <div class="email-toolbar col-lg-12">
            <!-- Start .email-toolbar -->
            <div class=pull-left role=toolbar>
              <button id=email-toggle class=email-toggle type=button><span class=sr-only>Toggle email nav</span> <span class=icon-bar></span> <span class=icon-bar></span> <span class=icon-bar></span></button>
              <a href=# class="btn btn-default btn-round btn-sm tip mr5" title="Refresh inbox"><i class="ec-refresh s16"></i></a> <a href=# class="btn btn-default btn-round btn-sm tip mr5" title=Reply><i class="im-undo s16"></i></a> <a href=# class="btn btn-default btn-round btn-sm tip mr5" title=Forward><i class="im-redo s16"></i></a> <a href=# class="btn btn-danger btn-round btn-sm tip mr5" title=Delete><i class="ec-trashcan s16"></i></a> <a href=# class="btn btn-default btn-round btn-sm tip mr5" title=Print><i class="fa-print s16"></i></a></div>
            <ul class=email-pager>
              <li class=pager-info>1-20 of 345</li>
              <li><a href=# class="btn btn-default btn-round btn-sm"><i class="en-arrow-left4 s16"></i></a></li>
              <li><a href=# class="btn btn-default btn-round btn-sm"><i class="en-arrow-right5 s16"></i></a></li>
            </ul>
          </div>
          <!-- End .email-toolbar -->
          <div class=email-read>
            <!-- Start .email-read -->
            <div class=email-header>
              <h3>Daily_Report1 - Daily Report Apr 27, 2014</h3>
            </div>
            <div class=email-info-bar>
              <div class=row>
                <div class="from col-lg-8">Monitor.Us / <a href=#><strong>no-reply@monitor.us</strong></a> to <a href=#><strong>me</strong></a></div>
                <div class="date col-lg-4 text-right"><span class=email-date>Apr 27 ( 2 days ago )</span></div>
              </div>
            </div>
            <div class=email-content>
              <!-- Start .email-content -->
              <!-- This is just example email text  -->
              <h4 class=text-center>Uptime monitors</h4>
              <table width=100% cellspacing=0 border=0>
                <tbody>
                  <tr>
                    <td colspan=7 style=color:#666666;font-size:13px;font-weight:bold;padding-top:15px>Monitoring Location : All 
                  <tr background="https://ci5.googleusercontent.com/proxy/Ap9-c87MgOxxcZj8F6NYc3sN5ObMNmiK4MG10ievYHP7y91qn7yRznHdkQ9QJ9Fawrjbdtvc__0bQ7oBWyb9wo4qsm6082_UV8Hmb2KJY_iLpN2zUtlyIosXlobe-Dghkw=s0-d-e1-ft#http://dashboard.monitis.com/images/weeklyreports/email_report_gradient.png">
                    <th background="https://ci5.googleusercontent.com/proxy/Ap9-c87MgOxxcZj8F6NYc3sN5ObMNmiK4MG10ievYHP7y91qn7yRznHdkQ9QJ9Fawrjbdtvc__0bQ7oBWyb9wo4qsm6082_UV8Hmb2KJY_iLpN2zUtlyIosXlobe-Dghkw=s0-d-e1-ft#http://dashboard.monitis.com/images/weeklyreports/email_report_gradient.png" style="height:23px;border-top:1px solid #dddddd;color:#666666;font-size:13px;font-weight:bold;text-align:left;padding:0px 2px;border-left:1px solid #dddddd;background-image:url('https://ci5.googleusercontent.com/proxy/Ap9-c87MgOxxcZj8F6NYc3sN5ObMNmiK4MG10ievYHP7y91qn7yRznHdkQ9QJ9Fawrjbdtvc__0bQ7oBWyb9wo4qsm6082_UV8Hmb2KJY_iLpN2zUtlyIosXlobe-Dghkw=s0-d-e1-ft#http://dashboard.monitis.com/images/weeklyreports/email_report_gradient.png');background-color:#f8f8f8">Test Name 
                    <th style="border-top:1px solid #dddddd;color:#666666;font-size:13px;font-weight:bold;text-align:left;padding:0px 2px;border-left:1px solid #dddddd;background-image:url('https://ci5.googleusercontent.com/proxy/Ap9-c87MgOxxcZj8F6NYc3sN5ObMNmiK4MG10ievYHP7y91qn7yRznHdkQ9QJ9Fawrjbdtvc__0bQ7oBWyb9wo4qsm6082_UV8Hmb2KJY_iLpN2zUtlyIosXlobe-Dghkw=s0-d-e1-ft#http://dashboard.monitis.com/images/weeklyreports/email_report_gradient.png');background-color:#f8f8f8">Type 
                    <th style="border-top:1px solid #dddddd;color:#666666;font-size:13px;font-weight:bold;text-align:left;padding:0px 2px;border-left:1px solid #dddddd;background-image:url('https://ci5.googleusercontent.com/proxy/Ap9-c87MgOxxcZj8F6NYc3sN5ObMNmiK4MG10ievYHP7y91qn7yRznHdkQ9QJ9Fawrjbdtvc__0bQ7oBWyb9wo4qsm6082_UV8Hmb2KJY_iLpN2zUtlyIosXlobe-Dghkw=s0-d-e1-ft#http://dashboard.monitis.com/images/weeklyreports/email_report_gradient.png');background-color:#f8f8f8">Group Name 
                    <th style="white-space:nowrap;border-top:1px solid #dddddd;color:#739b40;font-size:13px;font-weight:bold;text-align:right;padding:0px 2px;border-left:1px solid #dddddd;background-image:url('https://ci5.googleusercontent.com/proxy/Ap9-c87MgOxxcZj8F6NYc3sN5ObMNmiK4MG10ievYHP7y91qn7yRznHdkQ9QJ9Fawrjbdtvc__0bQ7oBWyb9wo4qsm6082_UV8Hmb2KJY_iLpN2zUtlyIosXlobe-Dghkw=s0-d-e1-ft#http://dashboard.monitis.com/images/weeklyreports/email_report_gradient.png');background-color:#f8f8f8">Uptime(%) 
                    <th style="white-space:nowrap;border-top:1px solid #dddddd;color:#666666;font-size:13px;font-weight:bold;text-align:right;padding:0px 2px;border-left:1px solid #dddddd;background-image:url('https://ci5.googleusercontent.com/proxy/Ap9-c87MgOxxcZj8F6NYc3sN5ObMNmiK4MG10ievYHP7y91qn7yRznHdkQ9QJ9Fawrjbdtvc__0bQ7oBWyb9wo4qsm6082_UV8Hmb2KJY_iLpN2zUtlyIosXlobe-Dghkw=s0-d-e1-ft#http://dashboard.monitis.com/images/weeklyreports/email_report_gradient.png');background-color:#f8f8f8">Avg Resp Time(ms) 
                    <th style="white-space:nowrap;border-top:1px solid #dddddd;color:#bd5757;font-size:13px;font-weight:bold;text-align:right;padding:0px 2px;border-right:1px solid #dddddd;border-left:1px solid #dddddd;background-image:url('https://ci5.googleusercontent.com/proxy/Ap9-c87MgOxxcZj8F6NYc3sN5ObMNmiK4MG10ievYHP7y91qn7yRznHdkQ9QJ9Fawrjbdtvc__0bQ7oBWyb9wo4qsm6082_UV8Hmb2KJY_iLpN2zUtlyIosXlobe-Dghkw=s0-d-e1-ft#http://dashboard.monitis.com/images/weeklyreports/email_report_gradient.png');background-color:#f8f8f8">Failures(#) 
                  <tr>
                    <td style="color:#127adb;font-size:13px;border-top:1px solid #dddddd;padding:4px 2px;border-left:1px solid #dddddd">suggeelson.com_http 
                    <td style="color:#666666;font-size:13px;border-top:1px solid #dddddd;padding:4px 2px;border-left:1px solid #dddddd">http 
                    <td style="color:#666666;font-size:13px;border-top:1px solid #dddddd;padding:4px 2px;border-left:1px solid #dddddd">Default 
                    <td style="color:#739b40;font-size:13px;text-align:right;border-top:1px solid #dddddd;padding:4px 2px;border-left:1px solid #dddddd">100 
                    <td style="color:#666666;font-size:13px;text-align:right;border-top:1px solid #dddddd;padding:4px 2px;border-left:1px solid #dddddd">1348.76 
                    <td style="color:#bd5757;font-size:13px;text-align:right;border-top:1px solid #dddddd;padding:4px 2px;border-right:1px solid #dddddd;border-left:1px solid #dddddd">0 
                  <tr>
                    <td style="color:#127adb;font-size:13px;border-top:1px solid #dddddd;padding:4px 2px;border-left:1px solid #dddddd"><a href=http://suggelab.com/ target=_blank>suggelab.com</a>
                    <td style="color:#666666;font-size:13px;border-top:1px solid #dddddd;padding:4px 2px;border-left:1px solid #dddddd">http 
                    <td style="color:#666666;font-size:13px;border-top:1px solid #dddddd;padding:4px 2px;border-left:1px solid #dddddd">Padrão 
                    <td style="color:#739b40;font-size:13px;text-align:right;border-top:1px solid #dddddd;padding:4px 2px;border-left:1px solid #dddddd">100 
                    <td style="color:#666666;font-size:13px;text-align:right;border-top:1px solid #dddddd;padding:4px 2px;border-left:1px solid #dddddd">147.44 
                    <td style="color:#bd5757;font-size:13px;text-align:right;border-top:1px solid #dddddd;padding:4px 2px;border-right:1px solid #dddddd;border-left:1px solid #dddddd">0 
                  <tr>
                    <td colspan=6 style="border-top:1px solid #dddddd">&nbsp; 
                  <tr>
                    <td colspan=7 style=color:#666666;font-size:13px;font-weight:bold;padding-top:15px>Monitoring Location : Germany 
                  <tr background="https://ci5.googleusercontent.com/proxy/Ap9-c87MgOxxcZj8F6NYc3sN5ObMNmiK4MG10ievYHP7y91qn7yRznHdkQ9QJ9Fawrjbdtvc__0bQ7oBWyb9wo4qsm6082_UV8Hmb2KJY_iLpN2zUtlyIosXlobe-Dghkw=s0-d-e1-ft#http://dashboard.monitis.com/images/weeklyreports/email_report_gradient.png">
                    <th background="https://ci5.googleusercontent.com/proxy/Ap9-c87MgOxxcZj8F6NYc3sN5ObMNmiK4MG10ievYHP7y91qn7yRznHdkQ9QJ9Fawrjbdtvc__0bQ7oBWyb9wo4qsm6082_UV8Hmb2KJY_iLpN2zUtlyIosXlobe-Dghkw=s0-d-e1-ft#http://dashboard.monitis.com/images/weeklyreports/email_report_gradient.png" style="height:23px;border-top:1px solid #dddddd;color:#666666;font-size:13px;font-weight:bold;text-align:left;padding:0px 2px;border-left:1px solid #dddddd;background-image:url('https://ci5.googleusercontent.com/proxy/Ap9-c87MgOxxcZj8F6NYc3sN5ObMNmiK4MG10ievYHP7y91qn7yRznHdkQ9QJ9Fawrjbdtvc__0bQ7oBWyb9wo4qsm6082_UV8Hmb2KJY_iLpN2zUtlyIosXlobe-Dghkw=s0-d-e1-ft#http://dashboard.monitis.com/images/weeklyreports/email_report_gradient.png');background-color:#f8f8f8">Test Name 
                    <th style="border-top:1px solid #dddddd;color:#666666;font-size:13px;font-weight:bold;text-align:left;padding:0px 2px;border-left:1px solid #dddddd;background-image:url('https://ci5.googleusercontent.com/proxy/Ap9-c87MgOxxcZj8F6NYc3sN5ObMNmiK4MG10ievYHP7y91qn7yRznHdkQ9QJ9Fawrjbdtvc__0bQ7oBWyb9wo4qsm6082_UV8Hmb2KJY_iLpN2zUtlyIosXlobe-Dghkw=s0-d-e1-ft#http://dashboard.monitis.com/images/weeklyreports/email_report_gradient.png');background-color:#f8f8f8">Type 
                    <th style="border-top:1px solid #dddddd;color:#666666;font-size:13px;font-weight:bold;text-align:left;padding:0px 2px;border-left:1px solid #dddddd;background-image:url('https://ci5.googleusercontent.com/proxy/Ap9-c87MgOxxcZj8F6NYc3sN5ObMNmiK4MG10ievYHP7y91qn7yRznHdkQ9QJ9Fawrjbdtvc__0bQ7oBWyb9wo4qsm6082_UV8Hmb2KJY_iLpN2zUtlyIosXlobe-Dghkw=s0-d-e1-ft#http://dashboard.monitis.com/images/weeklyreports/email_report_gradient.png');background-color:#f8f8f8">Group Name 
                    <th style="white-space:nowrap;border-top:1px solid #dddddd;color:#739b40;font-size:13px;font-weight:bold;text-align:right;padding:0px 2px;border-left:1px solid #dddddd;background-image:url('https://ci5.googleusercontent.com/proxy/Ap9-c87MgOxxcZj8F6NYc3sN5ObMNmiK4MG10ievYHP7y91qn7yRznHdkQ9QJ9Fawrjbdtvc__0bQ7oBWyb9wo4qsm6082_UV8Hmb2KJY_iLpN2zUtlyIosXlobe-Dghkw=s0-d-e1-ft#http://dashboard.monitis.com/images/weeklyreports/email_report_gradient.png');background-color:#f8f8f8">Uptime(%) 
                    <th style="white-space:nowrap;border-top:1px solid #dddddd;color:#666666;font-size:13px;font-weight:bold;text-align:right;padding:0px 2px;border-left:1px solid #dddddd;background-image:url('https://ci5.googleusercontent.com/proxy/Ap9-c87MgOxxcZj8F6NYc3sN5ObMNmiK4MG10ievYHP7y91qn7yRznHdkQ9QJ9Fawrjbdtvc__0bQ7oBWyb9wo4qsm6082_UV8Hmb2KJY_iLpN2zUtlyIosXlobe-Dghkw=s0-d-e1-ft#http://dashboard.monitis.com/images/weeklyreports/email_report_gradient.png');background-color:#f8f8f8">Avg Resp Time(ms) 
                    <th style="white-space:nowrap;border-top:1px solid #dddddd;color:#bd5757;font-size:13px;font-weight:bold;text-align:right;padding:0px 2px;border-right:1px solid #dddddd;border-left:1px solid #dddddd;background-image:url('https://ci5.googleusercontent.com/proxy/Ap9-c87MgOxxcZj8F6NYc3sN5ObMNmiK4MG10ievYHP7y91qn7yRznHdkQ9QJ9Fawrjbdtvc__0bQ7oBWyb9wo4qsm6082_UV8Hmb2KJY_iLpN2zUtlyIosXlobe-Dghkw=s0-d-e1-ft#http://dashboard.monitis.com/images/weeklyreports/email_report_gradient.png');background-color:#f8f8f8">Failures(#) 
                  <tr>
                    <td style="color:#127adb;font-size:13px;border-top:1px solid #dddddd;padding:4px 2px;border-left:1px solid #dddddd">suggeelson.com_http 
                    <td style="color:#666666;font-size:13px;border-top:1px solid #dddddd;padding:4px 2px;border-left:1px solid #dddddd">http 
                    <td style="color:#666666;font-size:13px;border-top:1px solid #dddddd;padding:4px 2px;border-left:1px solid #dddddd">Default 
                    <td style="color:#739b40;font-size:13px;text-align:right;border-top:1px solid #dddddd;padding:4px 2px;border-left:1px solid #dddddd">100 
                    <td style="color:#666666;font-size:13px;text-align:right;border-top:1px solid #dddddd;padding:4px 2px;border-left:1px solid #dddddd">1599.45 
                    <td style="color:#bd5757;font-size:13px;text-align:right;border-top:1px solid #dddddd;padding:4px 2px;border-right:1px solid #dddddd;border-left:1px solid #dddddd">0 
                  <tr>
                    <td style="color:#127adb;font-size:13px;border-top:1px solid #dddddd;padding:4px 2px;border-left:1px solid #dddddd"><a href=http://suggelab.com/ target=_blank>suggelab.com</a>
                    <td style="color:#666666;font-size:13px;border-top:1px solid #dddddd;padding:4px 2px;border-left:1px solid #dddddd">http 
                    <td style="color:#666666;font-size:13px;border-top:1px solid #dddddd;padding:4px 2px;border-left:1px solid #dddddd">Padrão 
                    <td style="color:#739b40;font-size:13px;text-align:right;border-top:1px solid #dddddd;padding:4px 2px;border-left:1px solid #dddddd">100 
                    <td style="color:#666666;font-size:13px;text-align:right;border-top:1px solid #dddddd;padding:4px 2px;border-left:1px solid #dddddd">205.21 
                    <td style="color:#bd5757;font-size:13px;text-align:right;border-top:1px solid #dddddd;padding:4px 2px;border-right:1px solid #dddddd;border-left:1px solid #dddddd">0 
                  <tr>
                    <td colspan=6 style="border-top:1px solid #dddddd">&nbsp; 
                  <tr>
                    <td colspan=7 style=color:#666666;font-size:13px;font-weight:bold;padding-top:15px>Monitoring Location : US-MID 
                  <tr background="https://ci5.googleusercontent.com/proxy/Ap9-c87MgOxxcZj8F6NYc3sN5ObMNmiK4MG10ievYHP7y91qn7yRznHdkQ9QJ9Fawrjbdtvc__0bQ7oBWyb9wo4qsm6082_UV8Hmb2KJY_iLpN2zUtlyIosXlobe-Dghkw=s0-d-e1-ft#http://dashboard.monitis.com/images/weeklyreports/email_report_gradient.png">
                    <th background="https://ci5.googleusercontent.com/proxy/Ap9-c87MgOxxcZj8F6NYc3sN5ObMNmiK4MG10ievYHP7y91qn7yRznHdkQ9QJ9Fawrjbdtvc__0bQ7oBWyb9wo4qsm6082_UV8Hmb2KJY_iLpN2zUtlyIosXlobe-Dghkw=s0-d-e1-ft#http://dashboard.monitis.com/images/weeklyreports/email_report_gradient.png" style="height:23px;border-top:1px solid #dddddd;color:#666666;font-size:13px;font-weight:bold;text-align:left;padding:0px 2px;border-left:1px solid #dddddd;background-image:url('https://ci5.googleusercontent.com/proxy/Ap9-c87MgOxxcZj8F6NYc3sN5ObMNmiK4MG10ievYHP7y91qn7yRznHdkQ9QJ9Fawrjbdtvc__0bQ7oBWyb9wo4qsm6082_UV8Hmb2KJY_iLpN2zUtlyIosXlobe-Dghkw=s0-d-e1-ft#http://dashboard.monitis.com/images/weeklyreports/email_report_gradient.png');background-color:#f8f8f8">Test Name 
                    <th style="border-top:1px solid #dddddd;color:#666666;font-size:13px;font-weight:bold;text-align:left;padding:0px 2px;border-left:1px solid #dddddd;background-image:url('https://ci5.googleusercontent.com/proxy/Ap9-c87MgOxxcZj8F6NYc3sN5ObMNmiK4MG10ievYHP7y91qn7yRznHdkQ9QJ9Fawrjbdtvc__0bQ7oBWyb9wo4qsm6082_UV8Hmb2KJY_iLpN2zUtlyIosXlobe-Dghkw=s0-d-e1-ft#http://dashboard.monitis.com/images/weeklyreports/email_report_gradient.png');background-color:#f8f8f8">Type 
                    <th style="border-top:1px solid #dddddd;color:#666666;font-size:13px;font-weight:bold;text-align:left;padding:0px 2px;border-left:1px solid #dddddd;background-image:url('https://ci5.googleusercontent.com/proxy/Ap9-c87MgOxxcZj8F6NYc3sN5ObMNmiK4MG10ievYHP7y91qn7yRznHdkQ9QJ9Fawrjbdtvc__0bQ7oBWyb9wo4qsm6082_UV8Hmb2KJY_iLpN2zUtlyIosXlobe-Dghkw=s0-d-e1-ft#http://dashboard.monitis.com/images/weeklyreports/email_report_gradient.png');background-color:#f8f8f8">Group Name 
                    <th style="white-space:nowrap;border-top:1px solid #dddddd;color:#739b40;font-size:13px;font-weight:bold;text-align:right;padding:0px 2px;border-left:1px solid #dddddd;background-image:url('https://ci5.googleusercontent.com/proxy/Ap9-c87MgOxxcZj8F6NYc3sN5ObMNmiK4MG10ievYHP7y91qn7yRznHdkQ9QJ9Fawrjbdtvc__0bQ7oBWyb9wo4qsm6082_UV8Hmb2KJY_iLpN2zUtlyIosXlobe-Dghkw=s0-d-e1-ft#http://dashboard.monitis.com/images/weeklyreports/email_report_gradient.png');background-color:#f8f8f8">Uptime(%) 
                    <th style="white-space:nowrap;border-top:1px solid #dddddd;color:#666666;font-size:13px;font-weight:bold;text-align:right;padding:0px 2px;border-left:1px solid #dddddd;background-image:url('https://ci5.googleusercontent.com/proxy/Ap9-c87MgOxxcZj8F6NYc3sN5ObMNmiK4MG10ievYHP7y91qn7yRznHdkQ9QJ9Fawrjbdtvc__0bQ7oBWyb9wo4qsm6082_UV8Hmb2KJY_iLpN2zUtlyIosXlobe-Dghkw=s0-d-e1-ft#http://dashboard.monitis.com/images/weeklyreports/email_report_gradient.png');background-color:#f8f8f8">Avg Resp Time(ms) 
                    <th style="white-space:nowrap;border-top:1px solid #dddddd;color:#bd5757;font-size:13px;font-weight:bold;text-align:right;padding:0px 2px;border-right:1px solid #dddddd;border-left:1px solid #dddddd;background-image:url('https://ci5.googleusercontent.com/proxy/Ap9-c87MgOxxcZj8F6NYc3sN5ObMNmiK4MG10ievYHP7y91qn7yRznHdkQ9QJ9Fawrjbdtvc__0bQ7oBWyb9wo4qsm6082_UV8Hmb2KJY_iLpN2zUtlyIosXlobe-Dghkw=s0-d-e1-ft#http://dashboard.monitis.com/images/weeklyreports/email_report_gradient.png');background-color:#f8f8f8">Failures(#) 
                  <tr>
                    <td style="color:#127adb;font-size:13px;border-top:1px solid #dddddd;padding:4px 2px;border-left:1px solid #dddddd">suggeelson.com_http 
                    <td style="color:#666666;font-size:13px;border-top:1px solid #dddddd;padding:4px 2px;border-left:1px solid #dddddd">http 
                    <td style="color:#666666;font-size:13px;border-top:1px solid #dddddd;padding:4px 2px;border-left:1px solid #dddddd">Default 
                    <td style="color:#739b40;font-size:13px;text-align:right;border-top:1px solid #dddddd;padding:4px 2px;border-left:1px solid #dddddd">100 
                    <td style="color:#666666;font-size:13px;text-align:right;border-top:1px solid #dddddd;padding:4px 2px;border-left:1px solid #dddddd">1098.06 
                    <td style="color:#bd5757;font-size:13px;text-align:right;border-top:1px solid #dddddd;padding:4px 2px;border-right:1px solid #dddddd;border-left:1px solid #dddddd">0 
                  <tr>
                    <td style="color:#127adb;font-size:13px;border-top:1px solid #dddddd;padding:4px 2px;border-left:1px solid #dddddd"><a href=http://suggelab.com/ target=_blank>suggelab.com</a>
                    <td style="color:#666666;font-size:13px;border-top:1px solid #dddddd;padding:4px 2px;border-left:1px solid #dddddd">http 
                    <td style="color:#666666;font-size:13px;border-top:1px solid #dddddd;padding:4px 2px;border-left:1px solid #dddddd">Padrão 
                    <td style="color:#739b40;font-size:13px;text-align:right;border-top:1px solid #dddddd;padding:4px 2px;border-left:1px solid #dddddd">100 
                    <td style="color:#666666;font-size:13px;text-align:right;border-top:1px solid #dddddd;padding:4px 2px;border-left:1px solid #dddddd">89.67 
                    <td style="color:#bd5757;font-size:13px;text-align:right;border-top:1px solid #dddddd;padding:4px 2px;border-right:1px solid #dddddd;border-left:1px solid #dddddd">0 
                  <tr>
                    <td colspan=6 style="border-top:1px solid #dddddd">&nbsp; 
              </table>
              <p style="color:#666666;margin-top:24px;font-size:11px;font-family:'Arial';line-height:19px;margin-left:auto;margin-right:auto;display:table;text-align:center">Copyright © 2013 - Monitor.Us. All rights reserved. All product and company names herein<br>
                may be trademarks of their respective owners. To the best of our knowledge, all details were<br>
                correct at the time of publishing; this information is subject to change without notice.</p>
              <!-- End of example email content -->
              <div class=attached-files>
                <!-- Start .attached-files -->
                <ul class=list-group>
                  <li class=list-group-item><span class="label label-danger mr10">PDF</span> <strong>Raport.pdf</strong>
                    <div class="btn-group pull-right"><a href=# class="btn btn-default">View</a> <a href=# class="btn btn-default">Download</a></div>
                  </li>
                  <li class=list-group-item><span class="label label-primary mr10">HTML</span> <strong>raport.html</strong>
                    <div class="btn-group pull-right"><a href=# class="btn btn-default">View</a> <a href=# class="btn btn-default">Download</a></div>
                  </li>
                </ul>
              </div>
              <!-- End .attached-files -->
            </div>
            <!-- End .email-content -->
          </div>
          <!-- End .email-read -->
        </div>
        <!-- End .email-wrapper -->
      </div>
      <!--End #email-content -->
      <!-- Page End here -->
    </div>
    <!-- End .outlet -->
  </div>
  <!-- End .content-wrapper -->
  <div class=clearfix></div>
</div>
<!-- End #content -->
<!-- Javascripts -->
<!-- Load pace first -->
<script src=assets/plugins/core/pace/pace.min.js></script>
<!-- Important javascript libs(put in all pages) -->
<script>window.jQuery || document.write('<script src="assets/js/libs/jquery-2.1.1.min.js">\x3C/script>')</script>
<script src=http://code.jquery.com/ui/1.10.4/jquery-ui.js></script>
<script>window.jQuery || document.write('<script src="assets/js/libs/jquery-ui-1.10.4.min.js">\x3C/script>')</script>
<!--[if lt IE 9]>
  <script type="text/javascript" src="assets/js/libs/excanvas.min.js"></script>
  <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <script type="text/javascript" src="assets/js/libs/respond.min.js"></script>
<![endif]-->
<script src=assets/js/pages/email.js></script>
<!-- Google Analytics:  -->
